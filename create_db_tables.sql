DROP DATABASE IF EXISTS `laws_db`;
CREATE SCHEMA `laws_db`; 

  CREATE TABLE `laws_db`.`laws` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `registration_num` VARCHAR(20) NULL,
  `name` VARCHAR(1500) NULL,
  `creation_date` DATE NULL,
  `url` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`registration_num`));
 
  CREATE TABLE `laws_db`.`factions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `f_id` INT,
  `f_name` VARCHAR(80) NULL,
  PRIMARY KEY(`id`),
  UNIQUE(`f_id`));

  CREATE TABLE `laws_db`.`f_dep` (
  `f_id` INT NOT NULL,
  `dep_id` INT NOT NULL,
  PRIMARY KEY(`f_id`,`dep_id`));

  CREATE TABLE `laws_db`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
   UNIQUE(`email`));

  CREATE TABLE `laws_db`.`deputies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `dep_id` INT,
  `name` VARCHAR(100),
  PRIMARY KEY (`id`),
  UNIQUE(`dep_id`));	

  CREATE TABLE `laws_db`.`rates` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `laws_db`.`likes` (
  `law_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `rate` INT NULL,
  PRIMARY KEY (`law_id`, `user_id`));

  CREATE TABLE `laws_db`.`law_deputies` (
  `deputy_id` INT NOT NULL,
  `law_id` INT NOT NULL,
  PRIMARY KEY(`deputy_id`,`law_id`));	

  CREATE TABLE `laws_db`.`updates` (
  `start_date` TIMESTAMP(6) NOT NULL,
  `result` TINYINT NULL,
  `info` VARCHAR(200) NULL,
  `vote_num` INT,
  `last_creation_date` DATE NULL,
  PRIMARY KEY (`start_date`));

  ALTER TABLE `laws_db`.`f_dep`
  ADD CONSTRAINT `fk_f_id`
  FOREIGN KEY (`f_id`)
  REFERENCES `laws_db`.`factions`(`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `laws_db`.`f_dep`
  ADD CONSTRAINT `fk_dep_id`
  FOREIGN KEY (`dep_id`)
  REFERENCES `laws_db`.`deputies`(`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `laws_db`.`law_deputies`
  ADD CONSTRAINT `fk_deputy_id`
  FOREIGN KEY (`deputy_id`)
  REFERENCES `laws_db`.`deputies`(`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

 ALTER TABLE `laws_db`.`law_deputies`
  ADD CONSTRAINT `fk_law_id`
  FOREIGN KEY (`law_id`)
  REFERENCES `laws_db`.`laws`(`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `laws_db`.`likes` 
  ADD CONSTRAINT `fk_likes_users`
  FOREIGN KEY (`user_id`)
  REFERENCES `laws_db`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
  
  ALTER TABLE `laws_db`.`likes` 
  ADD CONSTRAINT `fk_likes_laws`
  FOREIGN KEY (`law_id`)
  REFERENCES `laws_db`.`laws` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `laws_db`.`likes` 
  ADD CONSTRAINT `fk_likes_rates`
  FOREIGN KEY (`rate`)
  REFERENCES `laws_db`.`rates` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
  
  INSERT INTO `laws_db`.`rates` (`name`) VALUES ('like');
  INSERT INTO `laws_db`.`rates` (`name`) VALUES ('dislike');


