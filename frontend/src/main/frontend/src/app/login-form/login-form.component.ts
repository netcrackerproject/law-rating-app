import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'angular4-social-login';
import { SocialUser } from 'angular4-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angular4-social-login';
import { LawService } from '../services/law.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']  
})

export class LoginComponent {    
    
  private user: SocialUser;
  
  constructor(private lawService: LawService, private authService: AuthService, private router: Router) {
    
  }
  
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authService.authState.subscribe((user) => {
        this.user = user;
        if (this.user != null){                  
                this.router.navigate([this.lawService.lastUrl]);       
            }
        },    
        err => {
                    console.log('Login error!');
                }
        );
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID); 
    this.authService.authState.subscribe((user) => {        
        this.user = user;        
        if (this.user != null){                                    
            this.router.navigate([this.lawService.lastUrl]);
        }
    },
    err => {
                console.log('Login error!');
            }
    );
  }
  
  goBack(){      
    this.router.navigate([this.lawService.lastUrl]);
  }
}