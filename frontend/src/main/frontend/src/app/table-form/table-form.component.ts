import { Component, OnInit} from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Law } from '../Entity/law.entity';
import { Router } from '@angular/router';
import { LawService } from '../services/law.service';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import { AuthService } from 'angular4-social-login';
import { SocialUser } from 'angular4-social-login';

@Component({
    selector: 'app-table-form',
    templateUrl: './table-form.component.html',
    styleUrls: ['./table-form.component.css']
})

export class TableComponent implements OnInit {
               
    public laws: Law[];
    private mask: number = 0;
    public searchText: string;
    private user: SocialUser;    
    public logIn: boolean = false;
    title = 'Рейтинг законопроектов Государственной думы';      
    //initializing current page to one    

    isDesc: boolean = false;
    column: string = 'Date';
    direction: number;
    page: number = 1;
    
    pageChanged(offset) {
        this.page = offset;
        this.getData();
    }
          
    constructor(private http: HttpClient, private authService: AuthService,private router: Router, public lawService: LawService) {        
        
    }        
    
    ngOnInit():void {                         
        this.authService.authState.subscribe((user) => {
            this.user = user;
            this.getData();
        });                   
    }   
  
  getData(){           
        if (this.user != null){                            
            this.logIn = true;                                   
        } else {
            this.logIn = false;
        }           
        if (this.logIn === true){            
            this.lawService.getLawsById(this.user.email,this.page, this.mask).subscribe(res => this.laws = res);//this.laws = res);            
        } else if (this.logIn === false) {
            this.lawService.getLaws(this.page, this.mask).subscribe(res => this.laws = res);            
        }
  }    
  
  logInto(): void  {
      this.lawService.lastUrl='/';
      this.router.navigate(['/login']);
  }
  
  logOut(): void {            
      this.lawService.lastUrl='/';               
      this.logIn = false;      
      this.router.navigate(['/logout']);             
  }
  
  getUser(){            
      return this.user.email;
  }
  
  checkLog(){
      return this.logIn;
  }    
  
  like(_law: Law){      
        if (this.user != null){ 
            _law.like = this.lawService.like(_law,this.user.email.toString(), this.user.token);
        }      
    }
  
  dislike(_law: Law){
        if (this.user != null){              
            _law.like = this.lawService.disLike(_law,this.user.email.toString(), this.user.token);
        }      
    }
  
  sort(property){
    if (property === 'creationDate'){
        this.mask = 0; //for date
    } else if (property === 'name'){
        this.mask = 2; //for name
    }
    if (this.isDesc === false){
        this.mask += 1; //to last
    } else if (this.isDesc === true){
        this.mask += 0; //from last
    }
    this.isDesc = !this.isDesc; //change the direction    
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
    this.getData();
  };
  
  checkLike(_law: Law){            
        if (this.logIn === false){            
           return true;
        } 
        if (_law.like === 2){           
           return false;
        } else if (_law.like === 1){            
           return true
        }                           
        return false;
  }
  
  checkDislike(_law: Law){            
        if (this.logIn === false){            
           return true;
        }
        if (_law.like === 1){           
           return false;
        } else if (_law.like === 2){            
           return true
        }        
        return false;
  }    
  
  lawDetail(_id: string){      
      this.router.navigate(['law/'+_id]);
  }
}