import { Law } from '../Entity/law.entity';
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LawService {   
    public laws: Law[];
    public lastUrl: string;        
    
    public like(_law: Law, email: string, token: string): number{                    
            let body: any = {"lawId":_law.id, "email":email,"result": true};
            let jsonBody: any = JSON.stringify(body);                                      
            let headers = new HttpHeaders().set('Content-Type', 'application/json').set('X-XSRF-TOKEN', token);//.set('withCredentials', 'true');
            this.http.post(environment.baseUrl+'like', jsonBody, {headers: headers, responseType: 'text'})           
                .subscribe(
                  res => {                      
                      _law.like = 1;                                                                             
                  },
                      err => {
                      console.log(err);
                  }
                );   
                return _law.like;     
    };
    public disLike(_law: Law, email: string, token: string): number{
        let body: any = {"lawId":_law.id, "email":email,"result": false};
            let jsonBody: any = JSON.stringify(body);                                      
            let headers = new HttpHeaders().set('Content-Type', 'application/json').set('X-XSRF-TOKEN', token);
            this.http.post(environment.baseUrl+'like', jsonBody, {headers: headers, responseType: 'text' })           
                .subscribe(
                  res => {                                          
                        _law.like = 2;                                                                                                   
                  },
                      err => {
                      console.log(err);
                  }
                );
                return _law.like;
    };
    public getLaws(page: number, mask: number): Observable<Law[]>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Content-Encoding', 'gzip');
        let params = new HttpParams().set('page',page.toString()).set('mask',mask.toString());
	return this.http.get<Law[]>(environment.baseUrl+'laws', {headers:headers,params:params} ).map(data => {                    
                return data;                            
        });           
    };
    public getLawsById(email: string, page: number, mask: number): Observable<Law[]>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Content-Encoding', 'gzip');
        let params = new HttpParams().set('email', email).set('page',page.toString()).set('mask',mask.toString());
        return this.http.get<Law[]>(environment.baseUrl+'laws',{headers:headers, params:params}).map(data => {                                    
               return data;           
        });        
    };
    public getLawById(email: string,num: string): Observable<Law>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Content-Encoding', 'gzip');
        let params = new HttpParams().set('email', email).set('regNum', num);       
        return this.http.get<Law>(environment.baseUrl+'law',{headers:headers, params:params}).map(data => {                           
           return data;            
        });
    };
    public getLaw(num: string){
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Content-Encoding', 'gzip');
	let params = new HttpParams().set('regNum', num);
        return this.http.get<Law>(environment.baseUrl+'law',{headers:headers, params:params}).map(data => {
            return data;
        });
    };
    constructor(private http: HttpClient){  }
}