import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'angular4-social-login';
import { LawService } from '../services/law.service';

@Component({
    selector: 'app-logout',
    template: '',
    styleUrls: []
})

export class LogoutComponent implements OnInit {  
    
    constructor(private lawService: LawService, private authService: AuthService, private router: Router) {
        
    }
  
    ngOnInit() {
      /*  this.authService.authState.subscribe((user) => {
            this.user = user;            
        }); */        
        this.authService.signOut();                        
        this.router.navigate([this.lawService.lastUrl]);
    }
}
