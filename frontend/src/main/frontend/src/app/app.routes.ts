import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './login-form/login-form.component';
import { TableComponent } from './table-form/table-form.component';
import { LogoutComponent } from './logout/logout.component';
import { LawsFormComponent } from './laws-form/laws-form.component';

//rate
import { LawsRatingComponent } from './laws-rating/laws-rating-form/laws-rating-form.component'
import { LawsMinRateComponent } from './laws-rating/laws-minrate-form/laws-minrate-form.component'
import { LawsMaxRateComponent } from './laws-rating/laws-maxrate-form/laws-maxrate-form.component'

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'rating',component: LawsRatingComponent},
  {path: 'minrate',component: LawsMinRateComponent},
  {path: 'maxrate',component: LawsMaxRateComponent},
  {path: 'law/:id', component: LawsFormComponent},
  {path: '', component: TableComponent},
  {path: '**', component: TableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
