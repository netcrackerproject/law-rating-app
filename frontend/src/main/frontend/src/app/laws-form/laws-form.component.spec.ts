import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsFormComponentComponent } from './laws-form-component.component';

describe('LawsFormComponentComponent', () => {
  let component: LawsFormComponentComponent;
  let fixture: ComponentFixture<LawsFormComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawsFormComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsFormComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
