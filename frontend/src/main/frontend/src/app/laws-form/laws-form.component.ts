import { Component, OnInit } from '@angular/core';
import { LawService } from '../services/law.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Law } from '../Entity/law.entity';
import { AuthService } from 'angular4-social-login';
import { SocialUser } from 'angular4-social-login';
import {BrowserModule} from '@angular/platform-browser'
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Component({
  selector: 'app-laws-form',
  templateUrl: './laws-form.component.html',
  styleUrls: ['./laws-form.component.css']
})

export class LawsFormComponent {
            
    public law: Law;        
    private num: string;
    private user: SocialUser;
    private logIn: boolean = false;        
    public downloaded:boolean = false;
    
    constructor(private domSanitizer : DomSanitizer, private route: ActivatedRoute, private http: HttpClient, public authService: AuthService,private router: Router, private lawService: LawService) {
               
    }        

    getNumber(): string{
        return this.num;       
    }       

    ngOnInit():void {        
        this.num = this.route.snapshot.paramMap.get('id');                    
        this.authService.authState.subscribe((user) => {
            this.user = user;     
            this.getData()                       
        });                                                                 
    }                        
  
    getData(){
        if (this.user != null){                            
            this.logIn = true;                                   
        } else {
            this.logIn = false;
        }           
        if (this.logIn === true){            
            this.lawService.getLawById(this.user.email,this.num ).subscribe(res => this.law = res);//this.laws = res);            
        } else if (this.logIn === false) {
            this.lawService.getLaw(this.num).subscribe(res => this.law = res);            
        }                     
    }
        
    getTranscript(){
        return this.domSanitizer.bypassSecurityTrustResourceUrl(this.law.url);
    }    
        
    logInto(): void  {
        this.lawService.lastUrl='law/'+this.num;
        this.router.navigate(['/login']);
    }

    logOut(): void {      
      this.lawService.lastUrl='law/'+this.num;            
      this.logIn = false;          
      this.router.navigate(['/logout']);      
    }
  
  getUser(){            
      return this.user.email;
  }   
  
  checkLaw(){
      if (!this.law){
          return false;
      }
      return true;
  }
  
  checkLog(){
      return this.logIn;
  }    
  
  like(){
        if (this.user != null){             
            this.law.like = this.lawService.like(this.law,this.user.email.toString(), this.user.token);
        }      
    }
  
  dislike(){
        if (this.user != null){                         
            this.law.like = this.lawService.disLike(this.law,this.user.email.toString(), this.user.token);
        }      
    }
    
    checkLike(){            
        if (this.logIn === false || !this.law){            
           return true;
        } 
        if (this.law.like === 2){           
           return false;
        } else if (this.law.like === 1){            
           return true
        }                           
        return false;
  } 
  
  checkDislike(){            
        if (this.logIn === false || !this.law){            
           return true;
        }
        if (this.law.like === 1){           
           return false;
        } else if (this.law.like === 2){            
           return true
        }        
        return false;
  }        
  
  toRating(){      
      this.router.navigate(['rating']);      
  }   
}
