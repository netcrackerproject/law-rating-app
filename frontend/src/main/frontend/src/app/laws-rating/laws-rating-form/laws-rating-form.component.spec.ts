import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsRatingFormComponent } from './laws-rating-form.component';

describe('LawsRatingFormComponent', () => {
  let component: LawsRatingFormComponent;
  let fixture: ComponentFixture<LawsRatingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawsRatingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsRatingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
