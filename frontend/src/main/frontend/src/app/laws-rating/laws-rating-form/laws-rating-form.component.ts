import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Law } from '../../Entity/law.entity';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-laws-rating-form',
  templateUrl: './laws-rating-form.component.html',
  styleUrls: ['../css/laws-rate.component.css']
})
export class LawsRatingComponent implements OnInit {

    laws: Law[];
    
    constructor(private http: HttpClient, private router: Router) { }

    ngOnInit() {
      this.getLaws();
    }

    getLaws(): void {
          let params = new HttpParams().set('params', 'rate');
          this.http.get<Law[]>(environment.baseUrl+'rating',{ params: params }).subscribe(data => {
              //console.log(data);
              this.laws = data;              
              },
              err => {
                  console.log('Get request error!');
              }
          );
      }
      
    lawDetail(_id: string){
      this.router.navigate(['law/'+_id]);
    }   
}
