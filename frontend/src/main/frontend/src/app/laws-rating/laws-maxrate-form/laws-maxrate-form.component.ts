import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Law } from '../../Entity/law.entity';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-laws-maxrate-form',
  templateUrl: './laws-maxrate-form.component.html',
  styleUrls: ['../css/laws-rate.component.css']
})
export class LawsMaxRateComponent implements OnInit {

    laws: Law[];
    
    constructor(private http: HttpClient, private router: Router) { }

    ngOnInit() {
      this.getLaws();
    }

    getLaws(): void {
          let params = new HttpParams().set('params', 'max');
          this.http.get<Law[]>(environment.baseUrl+'rating',{ params: params }).subscribe(data => {
              //console.log(data);
              this.laws = data;              
              },
              err => {
                  console.log('Get request error!');
              }
          );
      }
    lawDetail(_id: string){
      this.router.navigate(['law/'+_id]);
    }
}


/*акцент на одну тему. Без общих фраз. Руководитель подчиненный(стиль управления, стимулирование).
 * Приём на работу, интервью с работодателем.
 * Телефонный разговор. 
 * Требования к служеному тел. разговору.
 * Служебная документация
 * Деловое общение(беседа). Методы спора, аргументирование.2
 * Иммидж мужчины, организации, женщины.
 * Планирование, тайм-менеджмент. ПРавило поведения при трудоустройстве.
 * Этикет в комьютерных сетях.
 * Стили переговоров. Необходимость знания своего партнера по общению. Создание блогоприятной атмосферы.
 * Учебник по этикету. Объём двойной листок.
 *
 *   */