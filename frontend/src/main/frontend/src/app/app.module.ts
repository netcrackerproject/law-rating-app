import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

/*paging*/
import {PaginationComponent} from './paging/paging.component'

/*import users component*/
import { AppComponent } from './app.component';
import { LoginComponent } from './login-form/login-form.component';
import { TableComponent } from './table-form/table-form.component';
import { LogoutComponent } from './logout/logout.component';
import { LawsFormComponent } from './laws-form/laws-form.component';

//laws rating
import { LawsRatingComponent } from './laws-rating/laws-rating-form/laws-rating-form.component'
import { LawsMinRateComponent } from './laws-rating/laws-minrate-form/laws-minrate-form.component'
import { LawsMaxRateComponent } from './laws-rating/laws-maxrate-form/laws-maxrate-form.component'
import { LawService } from './services/law.service';


/*imports for routing*/
import {AppRoutingModule} from './app.routes';

/*imports for login*/
import { SocialLoginModule } from "angular4-social-login";
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angular4-social-login';
import { AuthService } from 'angular4-social-login';

/*imports for page*/
import { FilterPipe } from './filterByName.pipe';
import { OrderByPipe } from './orderBy.pipe';
import {NgxPaginationModule} from 'ngx-pagination';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("884177262754-amrt34h4mmnajfljb0ojlcv7vbqd3tcc.apps.googleusercontent.com")
  }/*,
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("376516432782088")
  }*/
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LogoutComponent,   
    LoginComponent,   
    TableComponent, 
    FilterPipe,
    OrderByPipe,    
    LawsFormComponent,
    LawsRatingComponent,
    LawsMinRateComponent,
    LawsMaxRateComponent,
    PaginationComponent  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SocialLoginModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxPaginationModule    
    ],
    providers: [
    {          
        provide: AuthServiceConfig,
        useFactory: provideConfig      
    },    
    {
        provide: AuthService,
        useClass: AuthService
    },    
    {
        provide: LawService,
        useClass: LawService
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
