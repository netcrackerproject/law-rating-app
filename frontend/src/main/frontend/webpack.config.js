const fs = require('fs');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssUrl = require('postcss-url');
const cssnano = require('cssnano');
const CompressionPlugin = require("compression-webpack-plugin");
const { optimize } = require('webpack')

const { NoEmitOnErrorsPlugin, EnvironmentPlugin, HashedModuleIdsPlugin } = require('webpack');
const { BaseHrefWebpackPlugin, SuppressExtractedTextChunksWebpackPlugin } = require('@angular/cli/plugins/webpack');
const { CommonsChunkPlugin, ModuleConcatenationPlugin, UglifyJsPlugin } = require('webpack').optimize;
const { LicenseWebpackPlugin } = require('license-webpack-plugin');
const { AotPlugin } = require('@ngtools/webpack');

const nodeModules = path.join(process.cwd(), 'node_modules');
const realNodeModules = fs.realpathSync(nodeModules);
const genDirNodeModules = path.join(process.cwd(), 'src', '$$_gendir', 'node_modules');
const entryPoints = ["inline","polyfills","sw-register","styles","vendor","main"];
const minimizeCss = true;
const baseHref = "";
const deployUrl = "";
const postcssPlugins = function () {
        // safe settings based on: https://github.com/ben-eb/cssnano/issues/358#issuecomment-283696193
        const importantCommentRe = /@preserve|@license|[@#]\s*source(?:Mapping)?URL|^!/i;
        const minimizeOptions = {
            autoprefixer: false,
            safe: true,
            mergeLonghand: false,
            discardComments: { remove: (comment) => !importantCommentRe.test(comment) }
        };
        return [
            postcssUrl({
                url: (URL) => {
                    // Only convert root relative URLs, which CSS-Loader won't process into require().
                    if (!URL.startsWith('/') || URL.startsWith('//')) {
                        return URL;
                    }
                    if (deployUrl.match(/:\/\//)) {
                        // If deployUrl contains a scheme, ignore baseHref use deployUrl as is.
                        return `${deployUrl.replace(/\/$/, '')}${URL}`;
                    }
                    else if (baseHref.match(/:\/\//)) {
                        // If baseHref contains a scheme, include it as is.
                        return baseHref.replace(/\/$/, '') +
                            `/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                    }
                    else {
                        // Join together base-href, deploy-url and the original URL.
                        // Also dedupe multiple slashes into single ones.
                        return `/${baseHref}/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                    }
                }
            }),
            autoprefixer(),
        ].concat(minimizeCss ? [cssnano(minimizeOptions)] : []);
    };




module.exports = {
  "resolve": {
    "extensions": [
      ".ts",
      ".js"
    ],
    "modules": [
      "./node_modules",
      "./node_modules"
    ],
    "symlinks": true,
    "alias": {
      "rxjs/AsyncSubject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/AsyncSubject.js",
      "rxjs/BehaviorSubject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/BehaviorSubject.js",
      "rxjs/InnerSubscriber": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/InnerSubscriber.js",
      "rxjs/Notification": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Notification.js",
      "rxjs/Observable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Observable.js",
      "rxjs/Observer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Observer.js",
      "rxjs/Operator": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Operator.js",
      "rxjs/OuterSubscriber": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/OuterSubscriber.js",
      "rxjs/ReplaySubject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/ReplaySubject.js",
      "rxjs/Rx": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Rx.js",
      "rxjs/Scheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Scheduler.js",
      "rxjs/Subject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Subject.js",
      "rxjs/SubjectSubscription": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/SubjectSubscription.js",
      "rxjs/Subscriber": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Subscriber.js",
      "rxjs/Subscription": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/Subscription.js",
      "rxjs/add/observable/bindCallback": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/bindCallback.js",
      "rxjs/add/observable/bindNodeCallback": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/bindNodeCallback.js",
      "rxjs/add/observable/combineLatest": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/combineLatest.js",
      "rxjs/add/observable/concat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/concat.js",
      "rxjs/add/observable/defer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/defer.js",
      "rxjs/add/observable/dom/ajax": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/dom/ajax.js",
      "rxjs/add/observable/dom/webSocket": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/dom/webSocket.js",
      "rxjs/add/observable/empty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/empty.js",
      "rxjs/add/observable/forkJoin": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/forkJoin.js",
      "rxjs/add/observable/from": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/from.js",
      "rxjs/add/observable/fromEvent": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/fromEvent.js",
      "rxjs/add/observable/fromEventPattern": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/fromEventPattern.js",
      "rxjs/add/observable/fromPromise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/fromPromise.js",
      "rxjs/add/observable/generate": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/generate.js",
      "rxjs/add/observable/if": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/if.js",
      "rxjs/add/observable/interval": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/interval.js",
      "rxjs/add/observable/merge": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/merge.js",
      "rxjs/add/observable/never": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/never.js",
      "rxjs/add/observable/of": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/of.js",
      "rxjs/add/observable/onErrorResumeNext": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/onErrorResumeNext.js",
      "rxjs/add/observable/pairs": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/pairs.js",
      "rxjs/add/observable/race": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/race.js",
      "rxjs/add/observable/range": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/range.js",
      "rxjs/add/observable/throw": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/throw.js",
      "rxjs/add/observable/timer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/timer.js",
      "rxjs/add/observable/using": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/using.js",
      "rxjs/add/observable/zip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/observable/zip.js",
      "rxjs/add/operator/audit": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/audit.js",
      "rxjs/add/operator/auditTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/auditTime.js",
      "rxjs/add/operator/buffer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/buffer.js",
      "rxjs/add/operator/bufferCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/bufferCount.js",
      "rxjs/add/operator/bufferTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/bufferTime.js",
      "rxjs/add/operator/bufferToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/bufferToggle.js",
      "rxjs/add/operator/bufferWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/bufferWhen.js",
      "rxjs/add/operator/catch": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/catch.js",
      "rxjs/add/operator/combineAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/combineAll.js",
      "rxjs/add/operator/combineLatest": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/combineLatest.js",
      "rxjs/add/operator/concat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/concat.js",
      "rxjs/add/operator/concatAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/concatAll.js",
      "rxjs/add/operator/concatMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/concatMap.js",
      "rxjs/add/operator/concatMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/concatMapTo.js",
      "rxjs/add/operator/count": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/count.js",
      "rxjs/add/operator/debounce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/debounce.js",
      "rxjs/add/operator/debounceTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/debounceTime.js",
      "rxjs/add/operator/defaultIfEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/defaultIfEmpty.js",
      "rxjs/add/operator/delay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/delay.js",
      "rxjs/add/operator/delayWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/delayWhen.js",
      "rxjs/add/operator/dematerialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/dematerialize.js",
      "rxjs/add/operator/distinct": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/distinct.js",
      "rxjs/add/operator/distinctUntilChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/distinctUntilChanged.js",
      "rxjs/add/operator/distinctUntilKeyChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/distinctUntilKeyChanged.js",
      "rxjs/add/operator/do": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/do.js",
      "rxjs/add/operator/elementAt": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/elementAt.js",
      "rxjs/add/operator/every": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/every.js",
      "rxjs/add/operator/exhaust": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/exhaust.js",
      "rxjs/add/operator/exhaustMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/exhaustMap.js",
      "rxjs/add/operator/expand": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/expand.js",
      "rxjs/add/operator/filter": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/filter.js",
      "rxjs/add/operator/finally": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/finally.js",
      "rxjs/add/operator/find": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/find.js",
      "rxjs/add/operator/findIndex": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/findIndex.js",
      "rxjs/add/operator/first": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/first.js",
      "rxjs/add/operator/groupBy": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/groupBy.js",
      "rxjs/add/operator/ignoreElements": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/ignoreElements.js",
      "rxjs/add/operator/isEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/isEmpty.js",
      "rxjs/add/operator/last": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/last.js",
      "rxjs/add/operator/let": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/let.js",
      "rxjs/add/operator/map": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/map.js",
      "rxjs/add/operator/mapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/mapTo.js",
      "rxjs/add/operator/materialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/materialize.js",
      "rxjs/add/operator/max": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/max.js",
      "rxjs/add/operator/merge": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/merge.js",
      "rxjs/add/operator/mergeAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/mergeAll.js",
      "rxjs/add/operator/mergeMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/mergeMap.js",
      "rxjs/add/operator/mergeMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/mergeMapTo.js",
      "rxjs/add/operator/mergeScan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/mergeScan.js",
      "rxjs/add/operator/min": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/min.js",
      "rxjs/add/operator/multicast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/multicast.js",
      "rxjs/add/operator/observeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/observeOn.js",
      "rxjs/add/operator/onErrorResumeNext": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/onErrorResumeNext.js",
      "rxjs/add/operator/pairwise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/pairwise.js",
      "rxjs/add/operator/partition": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/partition.js",
      "rxjs/add/operator/pluck": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/pluck.js",
      "rxjs/add/operator/publish": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/publish.js",
      "rxjs/add/operator/publishBehavior": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/publishBehavior.js",
      "rxjs/add/operator/publishLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/publishLast.js",
      "rxjs/add/operator/publishReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/publishReplay.js",
      "rxjs/add/operator/race": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/race.js",
      "rxjs/add/operator/reduce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/reduce.js",
      "rxjs/add/operator/repeat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/repeat.js",
      "rxjs/add/operator/repeatWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/repeatWhen.js",
      "rxjs/add/operator/retry": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/retry.js",
      "rxjs/add/operator/retryWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/retryWhen.js",
      "rxjs/add/operator/sample": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/sample.js",
      "rxjs/add/operator/sampleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/sampleTime.js",
      "rxjs/add/operator/scan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/scan.js",
      "rxjs/add/operator/sequenceEqual": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/sequenceEqual.js",
      "rxjs/add/operator/share": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/share.js",
      "rxjs/add/operator/shareReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/shareReplay.js",
      "rxjs/add/operator/single": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/single.js",
      "rxjs/add/operator/skip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/skip.js",
      "rxjs/add/operator/skipLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/skipLast.js",
      "rxjs/add/operator/skipUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/skipUntil.js",
      "rxjs/add/operator/skipWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/skipWhile.js",
      "rxjs/add/operator/startWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/startWith.js",
      "rxjs/add/operator/subscribeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/subscribeOn.js",
      "rxjs/add/operator/switch": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/switch.js",
      "rxjs/add/operator/switchMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/switchMap.js",
      "rxjs/add/operator/switchMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/switchMapTo.js",
      "rxjs/add/operator/take": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/take.js",
      "rxjs/add/operator/takeLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/takeLast.js",
      "rxjs/add/operator/takeUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/takeUntil.js",
      "rxjs/add/operator/takeWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/takeWhile.js",
      "rxjs/add/operator/throttle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/throttle.js",
      "rxjs/add/operator/throttleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/throttleTime.js",
      "rxjs/add/operator/timeInterval": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/timeInterval.js",
      "rxjs/add/operator/timeout": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/timeout.js",
      "rxjs/add/operator/timeoutWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/timeoutWith.js",
      "rxjs/add/operator/timestamp": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/timestamp.js",
      "rxjs/add/operator/toArray": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/toArray.js",
      "rxjs/add/operator/toPromise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/toPromise.js",
      "rxjs/add/operator/window": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/window.js",
      "rxjs/add/operator/windowCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/windowCount.js",
      "rxjs/add/operator/windowTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/windowTime.js",
      "rxjs/add/operator/windowToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/windowToggle.js",
      "rxjs/add/operator/windowWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/windowWhen.js",
      "rxjs/add/operator/withLatestFrom": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/withLatestFrom.js",
      "rxjs/add/operator/zip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/zip.js",
      "rxjs/add/operator/zipAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/add/operator/zipAll.js",
      "rxjs/interfaces": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/interfaces.js",
      "rxjs/observable/ArrayLikeObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ArrayLikeObservable.js",
      "rxjs/observable/ArrayObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ArrayObservable.js",
      "rxjs/observable/BoundCallbackObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/BoundCallbackObservable.js",
      "rxjs/observable/BoundNodeCallbackObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/BoundNodeCallbackObservable.js",
      "rxjs/observable/ConnectableObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ConnectableObservable.js",
      "rxjs/observable/DeferObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/DeferObservable.js",
      "rxjs/observable/EmptyObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/EmptyObservable.js",
      "rxjs/observable/ErrorObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ErrorObservable.js",
      "rxjs/observable/ForkJoinObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ForkJoinObservable.js",
      "rxjs/observable/FromEventObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/FromEventObservable.js",
      "rxjs/observable/FromEventPatternObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/FromEventPatternObservable.js",
      "rxjs/observable/FromObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/FromObservable.js",
      "rxjs/observable/GenerateObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/GenerateObservable.js",
      "rxjs/observable/IfObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/IfObservable.js",
      "rxjs/observable/IntervalObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/IntervalObservable.js",
      "rxjs/observable/IteratorObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/IteratorObservable.js",
      "rxjs/observable/NeverObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/NeverObservable.js",
      "rxjs/observable/PairsObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/PairsObservable.js",
      "rxjs/observable/PromiseObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/PromiseObservable.js",
      "rxjs/observable/RangeObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/RangeObservable.js",
      "rxjs/observable/ScalarObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/ScalarObservable.js",
      "rxjs/observable/SubscribeOnObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/SubscribeOnObservable.js",
      "rxjs/observable/TimerObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/TimerObservable.js",
      "rxjs/observable/UsingObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/UsingObservable.js",
      "rxjs/observable/bindCallback": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/bindCallback.js",
      "rxjs/observable/bindNodeCallback": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/bindNodeCallback.js",
      "rxjs/observable/combineLatest": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/combineLatest.js",
      "rxjs/observable/concat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/concat.js",
      "rxjs/observable/defer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/defer.js",
      "rxjs/observable/dom/AjaxObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/dom/AjaxObservable.js",
      "rxjs/observable/dom/WebSocketSubject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/dom/WebSocketSubject.js",
      "rxjs/observable/dom/ajax": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/dom/ajax.js",
      "rxjs/observable/dom/webSocket": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/dom/webSocket.js",
      "rxjs/observable/empty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/empty.js",
      "rxjs/observable/forkJoin": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/forkJoin.js",
      "rxjs/observable/from": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/from.js",
      "rxjs/observable/fromEvent": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/fromEvent.js",
      "rxjs/observable/fromEventPattern": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/fromEventPattern.js",
      "rxjs/observable/fromPromise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/fromPromise.js",
      "rxjs/observable/generate": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/generate.js",
      "rxjs/observable/if": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/if.js",
      "rxjs/observable/interval": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/interval.js",
      "rxjs/observable/merge": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/merge.js",
      "rxjs/observable/never": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/never.js",
      "rxjs/observable/of": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/of.js",
      "rxjs/observable/onErrorResumeNext": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/onErrorResumeNext.js",
      "rxjs/observable/pairs": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/pairs.js",
      "rxjs/observable/race": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/race.js",
      "rxjs/observable/range": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/range.js",
      "rxjs/observable/throw": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/throw.js",
      "rxjs/observable/timer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/timer.js",
      "rxjs/observable/using": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/using.js",
      "rxjs/observable/zip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/observable/zip.js",
      "rxjs/operator/audit": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/audit.js",
      "rxjs/operator/auditTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/auditTime.js",
      "rxjs/operator/buffer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/buffer.js",
      "rxjs/operator/bufferCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/bufferCount.js",
      "rxjs/operator/bufferTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/bufferTime.js",
      "rxjs/operator/bufferToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/bufferToggle.js",
      "rxjs/operator/bufferWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/bufferWhen.js",
      "rxjs/operator/catch": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/catch.js",
      "rxjs/operator/combineAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/combineAll.js",
      "rxjs/operator/combineLatest": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/combineLatest.js",
      "rxjs/operator/concat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/concat.js",
      "rxjs/operator/concatAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/concatAll.js",
      "rxjs/operator/concatMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/concatMap.js",
      "rxjs/operator/concatMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/concatMapTo.js",
      "rxjs/operator/count": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/count.js",
      "rxjs/operator/debounce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/debounce.js",
      "rxjs/operator/debounceTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/debounceTime.js",
      "rxjs/operator/defaultIfEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/defaultIfEmpty.js",
      "rxjs/operator/delay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/delay.js",
      "rxjs/operator/delayWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/delayWhen.js",
      "rxjs/operator/dematerialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/dematerialize.js",
      "rxjs/operator/distinct": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/distinct.js",
      "rxjs/operator/distinctUntilChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/distinctUntilChanged.js",
      "rxjs/operator/distinctUntilKeyChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/distinctUntilKeyChanged.js",
      "rxjs/operator/do": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/do.js",
      "rxjs/operator/elementAt": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/elementAt.js",
      "rxjs/operator/every": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/every.js",
      "rxjs/operator/exhaust": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/exhaust.js",
      "rxjs/operator/exhaustMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/exhaustMap.js",
      "rxjs/operator/expand": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/expand.js",
      "rxjs/operator/filter": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/filter.js",
      "rxjs/operator/finally": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/finally.js",
      "rxjs/operator/find": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/find.js",
      "rxjs/operator/findIndex": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/findIndex.js",
      "rxjs/operator/first": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/first.js",
      "rxjs/operator/groupBy": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/groupBy.js",
      "rxjs/operator/ignoreElements": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/ignoreElements.js",
      "rxjs/operator/isEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/isEmpty.js",
      "rxjs/operator/last": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/last.js",
      "rxjs/operator/let": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/let.js",
      "rxjs/operator/map": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/map.js",
      "rxjs/operator/mapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/mapTo.js",
      "rxjs/operator/materialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/materialize.js",
      "rxjs/operator/max": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/max.js",
      "rxjs/operator/merge": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/merge.js",
      "rxjs/operator/mergeAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/mergeAll.js",
      "rxjs/operator/mergeMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/mergeMap.js",
      "rxjs/operator/mergeMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/mergeMapTo.js",
      "rxjs/operator/mergeScan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/mergeScan.js",
      "rxjs/operator/min": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/min.js",
      "rxjs/operator/multicast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/multicast.js",
      "rxjs/operator/observeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/observeOn.js",
      "rxjs/operator/onErrorResumeNext": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/onErrorResumeNext.js",
      "rxjs/operator/pairwise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/pairwise.js",
      "rxjs/operator/partition": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/partition.js",
      "rxjs/operator/pluck": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/pluck.js",
      "rxjs/operator/publish": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/publish.js",
      "rxjs/operator/publishBehavior": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/publishBehavior.js",
      "rxjs/operator/publishLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/publishLast.js",
      "rxjs/operator/publishReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/publishReplay.js",
      "rxjs/operator/race": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/race.js",
      "rxjs/operator/reduce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/reduce.js",
      "rxjs/operator/repeat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/repeat.js",
      "rxjs/operator/repeatWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/repeatWhen.js",
      "rxjs/operator/retry": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/retry.js",
      "rxjs/operator/retryWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/retryWhen.js",
      "rxjs/operator/sample": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/sample.js",
      "rxjs/operator/sampleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/sampleTime.js",
      "rxjs/operator/scan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/scan.js",
      "rxjs/operator/sequenceEqual": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/sequenceEqual.js",
      "rxjs/operator/share": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/share.js",
      "rxjs/operator/shareReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/shareReplay.js",
      "rxjs/operator/single": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/single.js",
      "rxjs/operator/skip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/skip.js",
      "rxjs/operator/skipLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/skipLast.js",
      "rxjs/operator/skipUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/skipUntil.js",
      "rxjs/operator/skipWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/skipWhile.js",
      "rxjs/operator/startWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/startWith.js",
      "rxjs/operator/subscribeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/subscribeOn.js",
      "rxjs/operator/switch": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/switch.js",
      "rxjs/operator/switchMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/switchMap.js",
      "rxjs/operator/switchMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/switchMapTo.js",
      "rxjs/operator/take": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/take.js",
      "rxjs/operator/takeLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/takeLast.js",
      "rxjs/operator/takeUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/takeUntil.js",
      "rxjs/operator/takeWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/takeWhile.js",
      "rxjs/operator/throttle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/throttle.js",
      "rxjs/operator/throttleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/throttleTime.js",
      "rxjs/operator/timeInterval": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/timeInterval.js",
      "rxjs/operator/timeout": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/timeout.js",
      "rxjs/operator/timeoutWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/timeoutWith.js",
      "rxjs/operator/timestamp": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/timestamp.js",
      "rxjs/operator/toArray": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/toArray.js",
      "rxjs/operator/toPromise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/toPromise.js",
      "rxjs/operator/window": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/window.js",
      "rxjs/operator/windowCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/windowCount.js",
      "rxjs/operator/windowTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/windowTime.js",
      "rxjs/operator/windowToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/windowToggle.js",
      "rxjs/operator/windowWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/windowWhen.js",
      "rxjs/operator/withLatestFrom": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/withLatestFrom.js",
      "rxjs/operator/zip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/zip.js",
      "rxjs/operator/zipAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operator/zipAll.js",
      "rxjs/operators/audit": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/audit.js",
      "rxjs/operators/auditTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/auditTime.js",
      "rxjs/operators/buffer": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/buffer.js",
      "rxjs/operators/bufferCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/bufferCount.js",
      "rxjs/operators/bufferTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/bufferTime.js",
      "rxjs/operators/bufferToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/bufferToggle.js",
      "rxjs/operators/bufferWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/bufferWhen.js",
      "rxjs/operators/catchError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/catchError.js",
      "rxjs/operators/combineAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/combineAll.js",
      "rxjs/operators/combineLatest": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/combineLatest.js",
      "rxjs/operators/concat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/concat.js",
      "rxjs/operators/concatAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/concatAll.js",
      "rxjs/operators/concatMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/concatMap.js",
      "rxjs/operators/concatMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/concatMapTo.js",
      "rxjs/operators/count": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/count.js",
      "rxjs/operators/debounce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/debounce.js",
      "rxjs/operators/debounceTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/debounceTime.js",
      "rxjs/operators/defaultIfEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/defaultIfEmpty.js",
      "rxjs/operators/delay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/delay.js",
      "rxjs/operators/delayWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/delayWhen.js",
      "rxjs/operators/dematerialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/dematerialize.js",
      "rxjs/operators/distinct": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/distinct.js",
      "rxjs/operators/distinctUntilChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/distinctUntilChanged.js",
      "rxjs/operators/distinctUntilKeyChanged": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/distinctUntilKeyChanged.js",
      "rxjs/operators/elementAt": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/elementAt.js",
      "rxjs/operators/every": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/every.js",
      "rxjs/operators/exhaust": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/exhaust.js",
      "rxjs/operators/exhaustMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/exhaustMap.js",
      "rxjs/operators/expand": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/expand.js",
      "rxjs/operators/filter": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/filter.js",
      "rxjs/operators/finalize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/finalize.js",
      "rxjs/operators/find": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/find.js",
      "rxjs/operators/findIndex": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/findIndex.js",
      "rxjs/operators/first": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/first.js",
      "rxjs/operators/groupBy": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/groupBy.js",
      "rxjs/operators/ignoreElements": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/ignoreElements.js",
      "rxjs/operators/index": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/index.js",
      "rxjs/operators/isEmpty": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/isEmpty.js",
      "rxjs/operators/last": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/last.js",
      "rxjs/operators/map": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/map.js",
      "rxjs/operators/mapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/mapTo.js",
      "rxjs/operators/materialize": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/materialize.js",
      "rxjs/operators/max": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/max.js",
      "rxjs/operators/merge": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/merge.js",
      "rxjs/operators/mergeAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/mergeAll.js",
      "rxjs/operators/mergeMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/mergeMap.js",
      "rxjs/operators/mergeMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/mergeMapTo.js",
      "rxjs/operators/mergeScan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/mergeScan.js",
      "rxjs/operators/min": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/min.js",
      "rxjs/operators/multicast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/multicast.js",
      "rxjs/operators/observeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/observeOn.js",
      "rxjs/operators/onErrorResumeNext": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/onErrorResumeNext.js",
      "rxjs/operators/pairwise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/pairwise.js",
      "rxjs/operators/partition": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/partition.js",
      "rxjs/operators/pluck": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/pluck.js",
      "rxjs/operators/publish": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/publish.js",
      "rxjs/operators/publishBehavior": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/publishBehavior.js",
      "rxjs/operators/publishLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/publishLast.js",
      "rxjs/operators/publishReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/publishReplay.js",
      "rxjs/operators/race": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/race.js",
      "rxjs/operators/reduce": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/reduce.js",
      "rxjs/operators/refCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/refCount.js",
      "rxjs/operators/repeat": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/repeat.js",
      "rxjs/operators/repeatWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/repeatWhen.js",
      "rxjs/operators/retry": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/retry.js",
      "rxjs/operators/retryWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/retryWhen.js",
      "rxjs/operators/sample": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/sample.js",
      "rxjs/operators/sampleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/sampleTime.js",
      "rxjs/operators/scan": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/scan.js",
      "rxjs/operators/sequenceEqual": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/sequenceEqual.js",
      "rxjs/operators/share": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/share.js",
      "rxjs/operators/shareReplay": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/shareReplay.js",
      "rxjs/operators/single": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/single.js",
      "rxjs/operators/skip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/skip.js",
      "rxjs/operators/skipLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/skipLast.js",
      "rxjs/operators/skipUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/skipUntil.js",
      "rxjs/operators/skipWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/skipWhile.js",
      "rxjs/operators/startWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/startWith.js",
      "rxjs/operators/subscribeOn": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/subscribeOn.js",
      "rxjs/operators/switchAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/switchAll.js",
      "rxjs/operators/switchMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/switchMap.js",
      "rxjs/operators/switchMapTo": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/switchMapTo.js",
      "rxjs/operators/take": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/take.js",
      "rxjs/operators/takeLast": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/takeLast.js",
      "rxjs/operators/takeUntil": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/takeUntil.js",
      "rxjs/operators/takeWhile": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/takeWhile.js",
      "rxjs/operators/tap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/tap.js",
      "rxjs/operators/throttle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/throttle.js",
      "rxjs/operators/throttleTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/throttleTime.js",
      "rxjs/operators/timeInterval": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/timeInterval.js",
      "rxjs/operators/timeout": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/timeout.js",
      "rxjs/operators/timeoutWith": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/timeoutWith.js",
      "rxjs/operators/timestamp": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/timestamp.js",
      "rxjs/operators/toArray": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/toArray.js",
      "rxjs/operators/window": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/window.js",
      "rxjs/operators/windowCount": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/windowCount.js",
      "rxjs/operators/windowTime": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/windowTime.js",
      "rxjs/operators/windowToggle": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/windowToggle.js",
      "rxjs/operators/windowWhen": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/windowWhen.js",
      "rxjs/operators/withLatestFrom": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/withLatestFrom.js",
      "rxjs/operators/zip": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/zip.js",
      "rxjs/operators/zipAll": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/zipAll.js",
      "rxjs/scheduler/Action": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/Action.js",
      "rxjs/scheduler/AnimationFrameAction": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AnimationFrameAction.js",
      "rxjs/scheduler/AnimationFrameScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AnimationFrameScheduler.js",
      "rxjs/scheduler/AsapAction": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AsapAction.js",
      "rxjs/scheduler/AsapScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AsapScheduler.js",
      "rxjs/scheduler/AsyncAction": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AsyncAction.js",
      "rxjs/scheduler/AsyncScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/AsyncScheduler.js",
      "rxjs/scheduler/QueueAction": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/QueueAction.js",
      "rxjs/scheduler/QueueScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/QueueScheduler.js",
      "rxjs/scheduler/VirtualTimeScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/VirtualTimeScheduler.js",
      "rxjs/scheduler/animationFrame": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/animationFrame.js",
      "rxjs/scheduler/asap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/asap.js",
      "rxjs/scheduler/async": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/async.js",
      "rxjs/scheduler/queue": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/scheduler/queue.js",
      "rxjs/symbol/iterator": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/symbol/iterator.js",
      "rxjs/symbol/observable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/symbol/observable.js",
      "rxjs/symbol/rxSubscriber": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/symbol/rxSubscriber.js",
      "rxjs/testing/ColdObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/ColdObservable.js",
      "rxjs/testing/HotObservable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/HotObservable.js",
      "rxjs/testing/SubscriptionLog": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/SubscriptionLog.js",
      "rxjs/testing/SubscriptionLoggable": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/SubscriptionLoggable.js",
      "rxjs/testing/TestMessage": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/TestMessage.js",
      "rxjs/testing/TestScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/testing/TestScheduler.js",
      "rxjs/util/AnimationFrame": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/AnimationFrame.js",
      "rxjs/util/ArgumentOutOfRangeError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/ArgumentOutOfRangeError.js",
      "rxjs/util/EmptyError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/EmptyError.js",
      "rxjs/util/FastMap": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/FastMap.js",
      "rxjs/util/Immediate": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/Immediate.js",
      "rxjs/util/Map": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/Map.js",
      "rxjs/util/MapPolyfill": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/MapPolyfill.js",
      "rxjs/util/ObjectUnsubscribedError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/ObjectUnsubscribedError.js",
      "rxjs/util/Set": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/Set.js",
      "rxjs/util/TimeoutError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/TimeoutError.js",
      "rxjs/util/UnsubscriptionError": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/UnsubscriptionError.js",
      "rxjs/util/applyMixins": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/applyMixins.js",
      "rxjs/util/assign": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/assign.js",
      "rxjs/util/errorObject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/errorObject.js",
      "rxjs/util/identity": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/identity.js",
      "rxjs/util/isArray": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isArray.js",
      "rxjs/util/isArrayLike": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isArrayLike.js",
      "rxjs/util/isDate": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isDate.js",
      "rxjs/util/isFunction": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isFunction.js",
      "rxjs/util/isNumeric": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isNumeric.js",
      "rxjs/util/isObject": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isObject.js",
      "rxjs/util/isPromise": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isPromise.js",
      "rxjs/util/isScheduler": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/isScheduler.js",
      "rxjs/util/noop": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/noop.js",
      "rxjs/util/not": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/not.js",
      "rxjs/util/pipe": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/pipe.js",
      "rxjs/util/root": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/root.js",
      "rxjs/util/subscribeToResult": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/subscribeToResult.js",
      "rxjs/util/toSubscriber": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/toSubscriber.js",
      "rxjs/util/tryCatch": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/util/tryCatch.js",
      "rxjs/operators": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/rxjs/_esm5/operators/index.js"
    }
  },
  "resolveLoader": {
    "modules": [
      "./node_modules",
      "./node_modules"
    ]
  },
  "entry": {
    "main": [
      "./src/main.ts"
    ],
    "polyfills": [
      "./src/polyfills.ts"
    ],
    "styles": [
      "./src/styles.css"
    ]
  },
  "output": {
    "path": path.join(process.cwd(), "dist"),
    "filename": "[name].[chunkhash:20].bundle.js",
    "chunkFilename": "[id].[chunkhash:20].chunk.js"
  },
  "module": {
    "rules": [
      {
        "enforce": "pre",
        "test": /\.js$/,
        "loader": "source-map-loader",
        "exclude": [
          /(\\|\/)node_modules(\\|\/)/
        ]
      },
      {
        "test": /\.html$/,
        "loader": "raw-loader"
      },
      {
        "test": /\.(eot|svg|cur)$/,
        "loader": "file-loader",
        "options": {
          "name": "[name].[hash:20].[ext]",
          "limit": 10000
        }
      },
      {
        "test": /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani)$/,
        "loader": "url-loader",
        "options": {
          "name": "[name].[hash:20].[ext]",
          "limit": 10000
        }
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.css$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.scss$|\.sass$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "sass-loader",
            "options": {
              "sourceMap": false,
              "precision": 8,
              "includePaths": []
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.less$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "less-loader",
            "options": {
              "sourceMap": false
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.styl$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "stylus-loader",
            "options": {
              "sourceMap": false,
              "paths": []
            }
          }
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.css$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    {
      "loader": "css-loader",
      "options": {
        "sourceMap": false,
        "importLoaders": 1
      }
    },
    {
      "loader": "postcss-loader",
      "options": {
        "ident": "postcss",
        "plugins": postcssPlugins
      }
    }
  ],
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.scss$|\.sass$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    {
      "loader": "css-loader",
      "options": {
        "sourceMap": false,
        "importLoaders": 1
      }
    },
    {
      "loader": "postcss-loader",
      "options": {
        "ident": "postcss",
        "plugins": postcssPlugins
      }
    },
    {
      "loader": "sass-loader",
      "options": {
        "sourceMap": false,
        "precision": 8,
        "includePaths": []
      }
    }
  ],
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.less$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    {
      "loader": "css-loader",
      "options": {
        "sourceMap": false,
        "importLoaders": 1
      }
    },
    {
      "loader": "postcss-loader",
      "options": {
        "ident": "postcss",
        "plugins": postcssPlugins
      }
    },
    {
      "loader": "less-loader",
      "options": {
        "sourceMap": false
      }
    }
  ],
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.styl$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    {
      "loader": "css-loader",
      "options": {
        "sourceMap": false,
        "importLoaders": 1
      }
    },
    {
      "loader": "postcss-loader",
      "options": {
        "ident": "postcss",
        "plugins": postcssPlugins
      }
    },
    {
      "loader": "stylus-loader",
      "options": {
        "sourceMap": false,
        "paths": []
      }
    }
  ],
  "publicPath": ""
})
      },
      {
        "test": /\.ts$/,
        "use": [
          "@ngtools/webpack"
        ]
      }
    ]
  },
  "plugins": [
      new optimize.UglifyJsPlugin({
        beautify: false,
        output: {
          comments: false
        },
        mangle: {
          screw_ie8: true
        },
        compress: {
          screw_ie8: true,
          warnings: false,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
          negate_iife: false
        }
      }),
      new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.js$|\.html$/,
            threshold: 10240,
            minRatio: 0.8
        }),
    new NoEmitOnErrorsPlugin(),
    new CopyWebpackPlugin([
      {
        "context": "src",
        "to": "",
        "from": {
          "glob": "assets/**/*",
          "dot": true
        }
      },
      {
        "context": "src",
        "to": "",
        "from": {
          "glob": "favicon.ico",
          "dot": true
        }
      }
    ], {
      "ignore": [
        ".gitkeep"
      ],
      "debug": "warning"
    }),
    new ProgressPlugin(),
    new CircularDependencyPlugin({
      "exclude": /(\\|\/)node_modules(\\|\/)/,
      "failOnError": false
    }),
    new HtmlWebpackPlugin({
      "template": "./src/index.html",
      "filename": "./index.html",
      "hash": false,
      "inject": true,
      "compile": true,
      "favicon": false,
      "minify": {
        "caseSensitive": true,
        "collapseWhitespace": true,
        "keepClosingSlash": true
      },
      "cache": true,
      "showErrors": true,
      "chunks": "all",
      "excludeChunks": [],
      "title": "Webpack App",
      "xhtml": true,
      "chunksSortMode": function sort(left, right) {
        let leftIndex = entryPoints.indexOf(left.names[0]);
        let rightindex = entryPoints.indexOf(right.names[0]);
        if (leftIndex > rightindex) {
            return 1;
        }
        else if (leftIndex < rightindex) {
            return -1;
        }
        else {
            return 0;
        }
    }
    }),
    new BaseHrefWebpackPlugin({}),
    new CommonsChunkPlugin({
      "name": [
        "inline"
      ],
      "minChunks": null
    }),
    new CommonsChunkPlugin({
      "name": [
        "vendor"
      ],
      "minChunks": (module) => {
                return module.resource
                    && (module.resource.startsWith(nodeModules)
                        || module.resource.startsWith(genDirNodeModules)
                        || module.resource.startsWith(realNodeModules));
            },
      "chunks": [
        "main"
      ]
    }),
    new CommonsChunkPlugin({
      "name": [
        "main"
      ],
      "minChunks": 2,
      "async": "common"
    }),
    new ExtractTextPlugin({
      "filename": "[name].[contenthash:20].bundle.css"
    }),
    new SuppressExtractedTextChunksWebpackPlugin(),
    new LicenseWebpackPlugin({
      "licenseFilenames": [
        "LICENSE",
        "LICENSE.md",
        "LICENSE.txt",
        "license",
        "license.md",
        "license.txt"
      ],
      "perChunkOutput": false,
      "outputTemplate": "/home/dmitriy/NetCracker/law-rating-app/frontend/src/main/frontend/node_modules/license-webpack-plugin/output.template.ejs",
      "outputFilename": "3rdpartylicenses.txt",
      "suppressErrors": true,
      "includePackagesWithoutLicense": false,
      "abortOnUnacceptableLicense": false,
      "addBanner": false,
      "bannerTemplate": "/*! 3rd party license information is available at <%- filename %> */",
      "includedChunks": [],
      "excludedChunks": [],
      "additionalPackages": [],
      "pattern": /^(MIT|ISC|BSD.*)$/
    }),
    new EnvironmentPlugin({
      "NODE_ENV": "production"
    }),
    new HashedModuleIdsPlugin({
      "hashFunction": "md5",
      "hashDigest": "base64",
      "hashDigestLength": 4
    }),
    new ModuleConcatenationPlugin({}),
    new UglifyJsPlugin({
      "mangle": {
        "screw_ie8": true
      },
      "compress": {
        "screw_ie8": true,
        "warnings": false
      },
      "output": {
        "ascii_only": true
      },
      "sourceMap": false,
      "comments": false
    }),
    new AotPlugin({
      "mainPath": "main.ts",
      "replaceExport": false,
      "hostReplacementPaths": {
        "environments/environment.ts": "environments/environment.prod.ts"
      },
      "exclude": [],
      "tsConfigPath": "src/tsconfig.app.json"
    })
  ],
  "node": {
    "fs": "empty",
    "global": true,
    "crypto": "empty",
    "tls": "empty",
    "net": "empty",
    "process": true,
    "module": false,
    "clearImmediate": false,
    "setImmediate": false
  },
  "devServer": {
    "historyApiFallback": true
  }
};
