package com.netcracker.daemon;

import com.netcracker.service.IDownloadService;
import com.netcracker.service.ILawService;
import com.netcracker.service.IStatService;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class LawDaemon {
    
    private final Logger log = LoggerFactory.getLogger(LawDaemon.class);
    private final SimpleDateFormat dateFormat;
    
    @Autowired
    private IDownloadService downloadService;
    
    @Autowired
    private ILawService lawService;
    
    @Autowired 
    private IStatService statService;
    
    public LawDaemon(){        
        dateFormat = new SimpleDateFormat("HH:mm:ss");     
    }
            
    @Scheduled(cron = "0 10 4 * * ?")
    public void downloadLaws() {                  
        log.info("Downloading is started. Time is now {}", dateFormat.format(new Date()));        
        downloadService.downloadLaws();
        log.info("Downloading was ended. The time is now {}", dateFormat.format(new Date()));
        statService.buildStat();
        lawService.clearCache();
    }        
}
