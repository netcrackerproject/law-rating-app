/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.DAO.MongoBD;

import org.json.simple.JSONObject;

/**
 *
 * @author dmitriy
 */
public interface IMongoDAO {
    
    /*
    http://api.duma.gov.ru/api/:token/deputy.json
    id - идентификатор депутата
    */
    void insertDeputy(JSONObject jsonObject); //Сведения о депутате +++
    
    /*
    http://api.duma.gov.ru/api/:token/search.json    
    */
    void insertLaw(JSONObject jsonObject); // +++
    
    /*
    http://api.duma.gov.ru/api/:token/transcript/:number.json
    number — номер законопроекта, по которому нужно получить стенограммы.
    */
    void insertTranscript(JSONObject jsonObject); //Стенограммы по законопроекту  +++
    
    /*
    http://api.duma.gov.ru/api/:token/classes.json
    
    */
    void insertClasses(JSONObject jsonObject); //Список отраслей законодательства  +++           
    
    /*
    http://api.duma.gov.ru/api/:token/transcriptDeputy/:deputy.json
    deputy - код депутата
    dateFrom - минимальная дата заседания
    dateTo - максимальная дата заседания
    name - название вопроса
    page — номер запрашиваемой страницы результатов, по умолчанию равно 1
    limit — количество результатов на странице, допустимые значения: 5, 10, 20 (по умолчанию)    
    */
    void insertTranscriptDeputy(JSONObject jsonObject); //Стенограммы выступлений депутата +++
    //add deputy id +++
    
    /*
    http://api.duma.gov.ru/api/:token/vote/:id.xml
    id - идентификатор голосования
    */
    void insertVote(JSONObject jsonObject); //Сведения о голосовании
    
    /*
    http://api.duma.gov.ru/api/:token/voteStats.json?faction=
    convocation - код созыва, в рамках которого осуществляется поиск
    from - минимальная дата заседания
    to - максимальная дата заседания
    faction - код фракции
    deputy - код депутата
    */
    void insertVoteStatsByDeputy(JSONObject jsonObject, long depId); //Статистика голосований ++
    //add faction id ++
    
    
    void insertVoteStatsByFaction(JSONObject jsonObject, long facId); //Статистика голосований ++
    //insert faction id ++
    
    /*
    http://api.duma.gov.ru/api/:token/topics.json
    */
    void insertTopics(JSONObject jsonObject);//Список тематических блоков +++
    
    /*
    http://api.duma.gov.ru/api/:token/voteSearch.json
    */
    void insertVoteSearch(JSONObject jsonObject); // Поиск голосований
    
    //create indexes for collections
    void createIndexes(); // Индексы для документов +++    
}
