/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.DAO.MongoBD;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.stereotype.Component;

/**
 *
 * @author dmitriy
 */
@Component
public class MongoDAO implements IMongoDAO{

    @Autowired
    private MongoTemplate mongoTemplate;                       
    
    @Override
    public void insertDeputy(JSONObject jsonObject) {    
        mongoTemplate.save(jsonObject, "deputies"); //+
    }

    @Override
    public void insertLaw(JSONObject jsonObject) {        
        mongoTemplate.save(jsonObject,"laws"); //+
    }

    @Override
    public void insertTranscript(JSONObject jsonObject) {
        mongoTemplate.save(jsonObject, "transcipt");  //+
    }

    @Override
    public void insertClasses(JSONObject jsonObject) {
        mongoTemplate.save(jsonObject, "classes"); //+
    }
    
    @Override
    public void insertTranscriptDeputy(JSONObject jsonObject) {
        mongoTemplate.save(jsonObject,"transcriptionDeputy"); //+
    }

    @Override
    public void insertVote(JSONObject jsonObject) {
        mongoTemplate.save(jsonObject,"votes"); //+
    }

    @Override
    public void insertVoteStatsByDeputy(JSONObject jsonObject, long depId) {
        jsonObject.put("id", depId);
        mongoTemplate.save(jsonObject,"voteByDeputy"); //+
    }
    
    @Override
    public void insertVoteStatsByFaction(JSONObject jsonObject, long facId) {
        jsonObject.put("id", facId);
        mongoTemplate.save(jsonObject,"voteByFaction"); //+
    }
    
    @Override
    public void insertTopics(JSONObject jsonObject){
         mongoTemplate.save(jsonObject,"topics"); //+
    }    

    @Override
    public void insertVoteSearch(JSONObject jsonObject) {       
        mongoTemplate.save(jsonObject,"voteSearch"); //+
    }

    @Override
    public void createIndexes() {
        mongoTemplate.indexOps("deputies").ensureIndex(new Index().on("id", Sort.Direction.DESC));
        mongoTemplate.indexOps("laws").ensureIndex(new Index().on("number", Sort.Direction.DESC));
        mongoTemplate.indexOps("classes").ensureIndex(new Index().on("id", Sort.Direction.DESC));
        mongoTemplate.indexOps("topics").ensureIndex(new Index().on("id", Sort.Direction.DESC));
        mongoTemplate.indexOps("transcipt").ensureIndex(new Index().on("number", Sort.Direction.DESC));
        mongoTemplate.indexOps("votes").ensureIndex(new Index().on("lawNumber", Sort.Direction.DESC));
        mongoTemplate.indexOps("voteSearch").ensureIndex(new Index().on("id", Sort.Direction.DESC));
        mongoTemplate.indexOps("voteByDeputy").ensureIndex(new Index().on("code", Sort.Direction.DESC).on("id",Sort.Direction.DESC));
        mongoTemplate.indexOps("voteByFaction").ensureIndex(new Index().on("id", Sort.Direction.DESC));
        mongoTemplate.indexOps("transcriptionDeputy").ensureIndex(new Index().on("id", Sort.Direction.DESC).on("page", Sort.Direction.DESC));
    }
}
