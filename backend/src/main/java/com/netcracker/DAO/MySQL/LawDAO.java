package com.netcracker.DAO.MySQL;

import com.netcracker.entity.Deputies;
import com.netcracker.entity.Law;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class LawDAO implements ILawDAO{           
    
    //for Laws
    private final String insertLaw = "Insert ignore into laws_db.laws (registration_num, name, creation_date, url) "
            + "values(?,?,?,?)";
    private final String selectLawsByDateDesc = "select l.* from "            
            +"laws_db.laws l inner join (select id from laws_db.laws order by creation_date desc limit 20 offset ?) q "
            + "on l.id = q.id;";
    private final String selectLawsByDateAsc = "select l.* from "            
            +"laws_db.laws l inner join (select id from laws_db.laws order by creation_date asc limit 20 offset ?) q "
            + "on l.id = q.id;";
    private final String selectLawsByNameDesc = "select l.* from "            
            + "laws_db.laws l inner join (select id from laws_db.laws order by name desc limit 20 offset ?) q on l.id = q.id;";
    private final String selectLawsByNameAsc = "select l.* from "            
            + "laws_db.laws l inner join (select id from laws_db.laws order by name asc limit 20 offset ?) q on l.id = q.id;";    
    private final String selectLaw = "select * from laws_db.laws where registration_num = ?;";
    
    //create link between law and deputies
    private final String insertLawDep = "insert ignore into laws_db.law_deputies(deputy_id,law_id) values(?,?)";
    
    //for Users
    private final String insertUser = "INSERT ignore INTO laws_db.users (email) VALUES (?)";    
    private final String checkUser = "select IFNULL((Select id from laws_db.users where email= ?),-1)";
    
    //for Likes
    private final String selectDislike = "select id from laws_db.rates where name = 'dislike'";
    private final String selectLike =  "select id from laws_db.rates where name = 'like'";
    private final String insertLikes = "Insert ignore into laws_db.likes (law_id, user_id, rate) values(?,?,?) "
                    +"ON DUPLICATE KEY UPDATE rate= ?";
    
    //for Deputies
    private final String checkDeputy = "select IFNULL("
            + "(Select id from laws_db.deputies where dep_id= ?),-1)";
    private final String insertDeputies = "insert ignore into laws_db.deputies(dep_id,name) values(?,?)";    
    
    //for factions
    private final String checkFaction = "select IFNULL((Select id from laws_db.factions where f_id = ?),-1)";
    private final String insertFaction = "insert ignore into factions(f_id,f_name) values(?,?)";
    private final String insertFacDep = "insert ignore into f_dep(f_id,dep_id) values(?,?)";
    
    /*for rating
    1 - like, 2 - dislike
        TODO add laws        
    */
    private final String getMaximum = "select l.*, rate from "
            + "(select law_id, count(rate) as rate from likes where rate = 1 "
            + "group by law_id order by count(law_id) desc limit 20) c inner join laws l on c.law_id = l.id;";
    private final String getMinimum = "select l.*, rate from "
            + "(select law_id, count(rate) as rate from likes where rate = 2 "
            + "group by law_id order by count(law_id) desc limit 20) c inner join laws l on c.law_id = l.id;";
    private final String getRate = "select l.id, l.registration_num, l.name, l.creation_date, l.url, "
            + "a.cnt-b.cnt as rate from "
            + "(select law_id, count(case when rate = 1 then 1 else NULL end) as cnt "
            + "from likes group by law_id) a inner join  "
            + "(select law_id, count(case when rate = 2 then 1 else NULL end) as cnt "
            + "from likes group by law_id) b on a.law_id = b.law_id "
            + "inner join laws l on l.id = a.law_id order by rate desc, a.cnt+b.cnt desc limit 20;";
   
    /*
    select likes for users
    */
    private final String selectLikesForUser = "select law_id, rate from laws_db.likes where user_id = ?";
    private final String selectLikedLaw = "select IFNULL((select rate from laws_db.likes where user_id = ? and law_id = ?),0)";
    
    @Autowired
    @Qualifier("mysql")
    private JdbcTemplate jdbcTemplate;         
    
    private final Logger log = LoggerFactory.getLogger(LawDAO.class);       
    KeyHolder holder;        
    
    public LawDAO(){
        holder = new GeneratedKeyHolder();        
    }
    
    @Override
    public void insertLawDep(long lawId, long depId){
        jdbcTemplate.update(insertLawDep,depId,lawId);
    }
    
    @Override
    public long addLaw(Law law){        
        //inserLaw        
        jdbcTemplate.update((Connection connection) -> {
            PreparedStatement ps = connection.prepareStatement(insertLaw, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, law.getRegistrationNumber());
            ps.setString(2, law.getName());
            ps.setDate(3, new java.sql.Date(law.getCreationDate().getTime()));
            ps.setString(4, law.getUrl());
            return ps;
        }, holder);
        return holder.getKey().longValue();        
    }
    
    @Override
    public List<Law> getLawsByDateDesc(int page) {
        List<Law> results = jdbcTemplate.query(selectLawsByDateDesc, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"),
                rs.getString("name"),
                rs.getDate("creation_date"),
                rs.getString("url")), new Object[]{20 * (page - 1)});
        return results;
    }

    @Override
    public List<Law> getLawsByDateAsc(int page) {
        List<Law> results = jdbcTemplate.query(selectLawsByDateAsc, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"),
                rs.getString("name"),
                rs.getDate("creation_date"),
                rs.getString("url")), new Object[]{20 * (page - 1)});
        return results;
    }

    @Override
    public List<Law> getLawsByNameDesc(int page) {
        List<Law> results = jdbcTemplate.query(selectLawsByNameDesc, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"),
                rs.getString("name"),
                rs.getDate("creation_date"),
                rs.getString("url")), new Object[]{20 * (page - 1)});
        return results;
    }

    @Override
    public List<Law> getLawsByNameAsc(int page) {
        List<Law> results = jdbcTemplate.query(selectLawsByNameAsc, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"),
                rs.getString("name"),
                rs.getDate("creation_date"),
                rs.getString("url")), new Object[]{20 * (page - 1)});
        return results;
    }
    
    @Override
    public Law getLaw(String regNum) {
        return jdbcTemplate.queryForObject(selectLaw, (ResultSet rs, int rowNum) -> new Law(
                    rs.getLong("id"),
                    rs.getString("registration_num"),
                    rs.getString("name"),
                    rs.getDate("creation_date"),
                    rs.getString("url")
            ), new Object[]{regNum});
    }

    private long insertUser(String email) {
        jdbcTemplate.update((Connection connection) -> {
            PreparedStatement ps = connection.prepareStatement(insertUser, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, email);
            return ps;
        }, holder); 
        return holder.getKey().longValue();
    }
 
    @Override
    public long likeLaw(long lawId, String email){
        try {
            long idEmail = jdbcTemplate.queryForObject(checkUser,new Object[]{email},Long.class);
            if (idEmail == -1){                       
                idEmail = insertUser(email);
            }            
            long likeId = jdbcTemplate.queryForObject(selectLike, Long.class);
            jdbcTemplate.update(insertLikes, lawId, idEmail, likeId, likeId);
        } catch(DataAccessException e){
            log.info(e.getMessage());
            return -1;
        }
        return 1;
    }
    
    @Override
    public int selectLikedLaw(String email, long lawId){
        long userId = jdbcTemplate.queryForObject(checkUser,new Object[]{email},Long.class);
        if (userId == -1){
            userId = insertUser(email);
        }           
        return jdbcTemplate.queryForObject(selectLikedLaw,new Object[]{userId, lawId},Integer.class);
    }
    
    @Override
    public long dislikeLaw(long lawId, String email){
        try {            
            long idEmail = jdbcTemplate.queryForObject(checkUser,new Object[]{email},Long.class);
            if (idEmail == -1){                    
                idEmail = insertUser(email);
            }
            log.info(Long.toString(idEmail));
            long likeId = jdbcTemplate.queryForObject(selectDislike, Long.class);
            jdbcTemplate.update(insertLikes,lawId, idEmail, likeId, likeId);
        } catch(DataAccessException e){
            log.info(e.getMessage());
            return -1;
        }
        return 1;               
    }    
    
    @Override
    public long addDeputie(Deputies deputies){                 
        jdbcTemplate.update((Connection connection) -> {
            PreparedStatement ps = connection.prepareStatement(insertDeputies, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, deputies.getId());
            ps.setString(2, deputies.getName());
            return ps;
        }, holder);
        return holder.getKey().longValue();
    }
    
    @Override
    public long checkDeputy(long depId){
        long idDeputies = jdbcTemplate.queryForObject(checkDeputy, new Object[]{depId}, Long.class);
        return idDeputies;
    }
    
    @Override
    public List<Law> getMin(){
        List<Law> results = jdbcTemplate.query(getMinimum, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"), 
                rs.getString("name"),                
                rs.getDate("creation_date"),
                rs.getString("url"), 
                rs.getLong("rate")));
        return results;    
    }
    
    @Override
    public List<Law> getMax(){
        List<Law> results = jdbcTemplate.query(getMaximum, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"), 
                rs.getString("name"),                
                rs.getDate("creation_date"),
                rs.getString("url"),                
                rs.getLong("rate")));
        return results;    
    }
    
    @Override
    public List<Law> countRating() {        
        List<Law> results = jdbcTemplate.query(getRate, (ResultSet rs, int rowNum) -> new Law(
                rs.getLong("id"),
                rs.getString("registration_num"), 
                rs.getString("name"),                
                rs.getDate("creation_date"),
                rs.getString("url"),
                rs.getLong("rate")));
        return results;
    }
        
    //select likes for users
    @Override
    public List selectLikedLaws(String email){
        long userId = jdbcTemplate.queryForObject(checkUser,new Object[]{email},Long.class);
        if (userId == -1){
            userId = insertUser(email);
        }
        return jdbcTemplate.queryForList(selectLikesForUser,userId);        
    }
    
    @Override
    public void insertFaction(long facId, String facName){ 
        //insert
        jdbcTemplate.update(insertFaction, facId, facName);        
    }
    
    @Override
    public void insertFacDep(long facId, String name, long depId){
        long fId = jdbcTemplate.queryForObject(checkFaction,new Object[]{facId}, Long.class);
        long dId = checkDeputy(depId);
        jdbcTemplate.update(insertFacDep,fId,dId);
    }
    
    @Override
    public long checkFaction(long facId){
        return jdbcTemplate.queryForObject(checkFaction,new Object[]{facId}, Long.class);
    }
}