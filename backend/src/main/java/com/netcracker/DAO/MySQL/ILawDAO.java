package com.netcracker.DAO.MySQL;

import com.netcracker.entity.Deputies;
import com.netcracker.entity.Law;
import java.util.List;

public interface ILawDAO {
   long addLaw(Law law);
   List<Law> getLawsByDateDesc(int page);
   List<Law> getLawsByDateAsc(int page);
   List<Law> getLawsByNameDesc(int page);
   List<Law> getLawsByNameAsc(int page);
   Law getLaw(String regNum); 
   long likeLaw(long lawId, String email);
   long dislikeLaw(long lawId, String email);   
   List<Law> countRating();
   List<Law> getMin();
   List<Law> getMax();
   long addDeputie(Deputies deputies);
   long checkDeputy(long depId);
   List selectLikedLaws(String name);
   int selectLikedLaw(String email, long lawId);
   void insertLawDep(long lawId, long depId);
   void insertFaction(long facId, String name);
   void insertFacDep(long facId, String name, long depId); 
   long checkFaction(long facId);
}
