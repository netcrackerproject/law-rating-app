/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.DAO.MySQL;

import com.netcracker.entity.DaemonResult;
import java.sql.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dmitriy
 */
@Repository
public class DaemonDAO implements IDaemonDAO{

    //for daemon
    private final String selectLast = "select ifnull("
            + "(select last_creation_date from laws_db.updates where result = 1 "
            + "order by start_date desc limit 1),str_to_date('1972-01-01','%Y-%m-%d'));";
    private final String insertResult = "insert into "
            + "laws_db.updates(start_date,result,info,last_creation_date) values(?,?,?,?)";
    
    @Autowired
    @Qualifier("mysql")
    private JdbcTemplate jdbcTemplate;
    
    private final Logger log = LoggerFactory.getLogger(DaemonDAO.class);  
    
    @Override
    public void insertResult(DaemonResult result) {
        try {
            jdbcTemplate.update(insertResult, result.getStart_date(), result.getResult(), result.getInfo(), result.getLast_creation_date());
        } catch (DataAccessException e){
                log.error(e.getMessage());
        }        
    }

    @Override
    public Date getResult() {
        Date data = jdbcTemplate.queryForObject(selectLast,Date.class);
        return data;
    }
    
}
