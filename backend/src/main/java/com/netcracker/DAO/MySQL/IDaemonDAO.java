/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.DAO.MySQL;

import com.netcracker.entity.DaemonResult;
import java.util.Date;

/**
 *
 * @author dmitriy
 */
public interface IDaemonDAO {
    void insertResult(DaemonResult result);
    Date getResult();
}
