/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.service;

import com.netcracker.DAO.MongoBD.IMongoDAO;
import com.netcracker.DAO.MySQL.IDaemonDAO;
import com.netcracker.DAO.MySQL.ILawDAO;
import com.netcracker.entity.DaemonResult;
import com.netcracker.entity.Deputies;
import com.netcracker.entity.Law;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.AsyncRestTemplate;
/*
 *
 * @author dmitriy
 *              JSON format 
 * laws{[number:"",name:"",introductionDate:"",url:"",subject:{deputies:{[name:""]}}}
 * 
 * 
 */
@Service
public class DownloadService implements IDownloadService{

    //tokens for api
    private final String url = "http://api.duma.gov.ru/api/";
    private final String token = "2cb5c6237bc1a35b66203ad184f108b07af08b57"; //"407881b85244275ce6f2b9c2c2b34dde88cb11e5";
    private final String appToken = "app_token=app871c5b8666b378328c1d5d5ff504f93df91dba60"; //appe905fe85729df908c34855830820d38424fe60fa";    
    private String searchUrl = url+token+"/search.json?"+appToken+"&registration_start="; 
    private final String getDeputyUrl = url+token+"/deputy.json?"+appToken+"&id=";
    private final String getTranscriptUrl = url+token+"/transcript/"; //+lawId.json?+appToken;
    private final String classesUrl = url+token+"/classes.json?"+appToken;           
    private final String getTrascriptDeputy = url+token+"/transcriptDeputy/"; //+deputyId.json?+appToken+&page=
    private final String topicsUrl = url+token+"/topics.json?"+appToken;
    private final String getFactionVotesUrl = url+token+"/voteStats.json?"+appToken+"&faction="; //id    
    
    // результат голосования по законопроекту
    private final String getVotesResultByLaw = url+token+"/voteSearch.json?"+appToken+"&number="; //+шв
    private final String getVotes = url+token+"/vote/"; // +session-id.json?+appToken
    
    
    int respCount = 0;
    
    //for creation http request       
    private final AsyncRestTemplate rest;
    
    private final HttpHeaders headers;
    private HttpStatus status;
    
    @Autowired
    private IMongoDAO mongoDAO;
    
    @Autowired
    private ILawDAO lawDAO;  
    
    @Autowired
    private IDaemonDAO daemonDAO;
    
    private final DaemonResult result;            
    
    //for parsing JSON
    private ResponseEntity<String> responseEntity;
    private final JSONParser parser;
    private Deputies dep;
    private Law law;
    private JSONObject obj; //for containing json object
    private JSONArray jArr, innerArr, tempArr; // for containing laws array and deputies array
    private String tempName;  //for multiple Authors 
        
    private final SimpleDateFormat formatter;    
    private final Logger log = LoggerFactory.getLogger(DownloadService.class);
    
    public DownloadService(){ 
        this.dep = new Deputies();
        this.result = new DaemonResult();
        //for parsin JSON
        this.law = new Law();
        this.parser = new JSONParser();
        this.obj = null;
        this.jArr = null;
        this.innerArr = null;
        
        formatter = new SimpleDateFormat("yyyy-MM-dd");
         //for creation http request  
        
        rest = new AsyncRestTemplate();        
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        rest.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
    }            
    
    //insert law topics +++
    @Override
    public void insertTopics() throws ParseException {        
        try {
            if (result.getLast_creation_date().compareTo(formatter.parse("1993-12-23")) == 0) {
                mongoDAO.createIndexes();
                ++respCount;
                do {
                    try {
                        Thread.sleep(300);
                        ListenableFuture<ResponseEntity<String>> listener = rest.exchange(topicsUrl, HttpMethod.GET, null, String.class);
                        responseEntity = listener.get(5,TimeUnit.SECONDS);
                    } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                        log.error(e.getMessage());
                    }
                } while (responseEntity.getStatusCode() != HttpStatus.OK);
                tempArr = (JSONArray) parser.parse(responseEntity.getBody());
                for (Object o :tempArr) {
                    mongoDAO.insertTopics((JSONObject) o);
                }
            }
        } catch (java.text.ParseException ex) {
            java.util.logging.Logger.getLogger(DownloadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.info("insertTopics {}", new Date());
    }
        
    //insert Classes +++
    @Override
    public void insertClasses() throws ParseException {        
        try {
            if (result.getLast_creation_date().compareTo(formatter.parse("1993-12-23")) == 0) {
                ++respCount;
                do {
                    try {
                        Thread.sleep(300);
                        ListenableFuture<ResponseEntity<String>> listener = rest.exchange(classesUrl, HttpMethod.GET, null, String.class);
                        responseEntity = listener.get(5, TimeUnit.SECONDS);
                    } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                        log.error(e.getMessage());
                    }
                } while (responseEntity.getStatusCode() != HttpStatus.OK);
                tempArr = (JSONArray) parser.parse(responseEntity.getBody());
                for (Object o: tempArr){
                    mongoDAO.insertClasses((JSONObject)o);
                }
            }
            log.info("insertClasses {}", new Date());
        } catch (java.text.ParseException ex) {
            java.util.logging.Logger.getLogger(DownloadService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    //for law transcript +++
    @Override
    public void getLawTranscript(String id) throws ParseException { 
        ++respCount;
        do {
            try {
                Thread.sleep(300);
                ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getTranscriptUrl + id + ".json?" + appToken, HttpMethod.GET, null, String.class);
                responseEntity = listener.get(5, TimeUnit.SECONDS);
            } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                log.error(e.getMessage());
            }
        } while (responseEntity.getStatusCode() != HttpStatus.OK);   
        obj = (JSONObject) parser.parse(responseEntity.getBody());
        log.info("getLawTranscript "+ getTranscriptUrl+id+".json?"+appToken);
        mongoDAO.insertTranscript(obj);
        log.info("getLawTranscript {}", new Date());
    }
    
    //for saving deputy to mongo +++
    @Override
    public int getDeputy(long id) throws ParseException {        
        ++respCount;
        do {
            try {
                Thread.sleep(300);
                ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getDeputyUrl + id, HttpMethod.GET, null, String.class);
                responseEntity = listener.get(5,TimeUnit.SECONDS);
            } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                log.error(e.getMessage());
            }
        } while (responseEntity.getStatusCode() != HttpStatus.OK);
        obj = null;
        obj = (JSONObject) parser.parse(responseEntity.getBody());
        if (obj == null || obj.isEmpty() || obj.toString().equals("null"))
            return -1;     
        if (!obj.containsKey("factionId") || !obj.containsKey("factionName") || !obj.containsKey("id")){
            return -1;
        }
        log.info("getDeputy "+ getDeputyUrl + id);
        mongoDAO.insertDeputy(obj);
        addVotesByFaction(Long.parseLong((String)obj.get("factionId")),(String)obj.get("factionName"),
                Long.parseLong((String)obj.get("id"))); 
        return 0;
    }
        
    //for saving deputy transcript +++
    @Override
    public void insertTranscriptDeputy(long id) throws ParseException {        
        int page = 1;
        do {                        
            ++respCount;                        
            do {
                try {                    
                    Thread.sleep(500);
                    ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getTrascriptDeputy+id+".json?"+appToken+"&page="+page, HttpMethod.GET, null, String.class);                
                    responseEntity = listener.get(5,TimeUnit.SECONDS);
                    obj = null;
                    obj = (JSONObject) parser.parse(responseEntity.getBody());
                } catch (RestClientException | ExecutionException | InterruptedException | TimeoutException e){
                    log.error("Connection error!");
                    log.info(e.getMessage());
                    log.info(getTrascriptDeputy+id+".json?"+appToken+"&page="+page);
                    try {
                        Thread.sleep(700);
                    } catch (InterruptedException ex) {
                        log.info(e.getMessage());
                    }
                }
            } while (responseEntity.getStatusCode() != HttpStatus.OK);  
            log.info("insertTranscriptDeputy {}", new Date());
            if (obj == null){
                break;
            }
            tempArr = (JSONArray)obj.get("meetings");
            if (tempArr == null || tempArr.isEmpty()){
                break;
            }
            obj.put("id", id);
            mongoDAO.insertTranscriptDeputy(obj);
            ++page;               
        } while(true);        
    }
    
    //for saving faction and deputies votes summary before deputy
    //caled id from information about deputy
    @Override
    public void addVotesByFaction(long faction_id, String fName, long depId) throws ParseException { 
        log.info("get Votes {}", new Date());
        if (lawDAO.checkFaction(faction_id) == -1){ //fi no faction download vtes for faction and deputies
            ++respCount;
            do {
                try {
                    Thread.sleep(300);
                    ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getFactionVotesUrl + faction_id, HttpMethod.GET, null, String.class);
                    responseEntity = listener.get(5, TimeUnit.SECONDS);
                    obj = null;
                } catch(TimeoutException | RestClientException | ExecutionException | InterruptedException e){
                    log.error(e.getMessage());
                }
            } while (responseEntity.getStatusCode() != HttpStatus.OK);
            obj = (JSONObject) parser.parse(responseEntity.getBody());      
            lawDAO.insertFaction(faction_id, fName);
            mongoDAO.insertVoteStatsByFaction((JSONObject)obj.get("faction"), faction_id);            
            tempArr = (JSONArray)obj.get("factionDeputy");
            addVotesForDeputy();
        }                   
        lawDAO.insertFacDep(faction_id, fName, depId); 
        log.info("get Votes {}", new Date());
    }    
    
    //insert deputies votes summary
    private void addVotesForDeputy(){
        for (Object o : tempArr) {
            mongoDAO.insertVoteStatsByDeputy((JSONObject)o, Long.parseLong((String)((JSONObject)o).get("code")));
        }        
    }
    
    //parse Deputies from laws
    private void parseInner(long lawId) throws ParseException {        
        if (!innerArr.isEmpty()) {          
            for (Object o : innerArr) {              
                dep.setName((String) ((JSONObject) o).get("name"));
                dep.setId(Long.parseLong((String)((JSONObject) o).get("id")));
                long depId = lawDAO.checkDeputy(dep.getId());
                if (depId == -1){ //if no deputy in table insert it into mongo; -1 is not in table
                    depId = lawDAO.addDeputie(dep);
                    if (getDeputy(dep.getId()) != -1) //mongo Info about deputy                    
                        insertTranscriptDeputy(dep.getId()); //mongo deputy transcript                                     
                }                
                lawDAO.insertLawDep(lawId,depId);
            log.info("parse inner inside loop {}", new Date());
            }
        }
    }
    
    //parse json
    private void parseJson() throws java.text.ParseException, ParseException {
        for (Object o : jArr) {    
            obj = (JSONObject) o;
            mongoDAO.insertLaw(obj); //add law object to our MONGO
            law.setRegistrationNumber((String) obj.get("number"));
            law.setName((String) obj.get("name"));
            tempName = (String) obj.get("introductionDate");
            if (tempName != null){
                law.setCreationDate(formatter.parse(tempName));
                if (law.getCreationDate().after(result.getLast_creation_date())) { //compare dates
                    result.setLast_creation_date(law.getCreationDate());
                }
            } else {
                law.setCreationDate(formatter.parse("1992-01-01"));
            }
            law.setUrl((String) obj.get("url"));
            innerArr = (JSONArray) ((JSONObject) obj.get("subject")).get("deputies"); // depities are in subject
            long lawId = lawDAO.addLaw(law);
            parseInner(lawId); //parse deputies         
            getLawTranscript(law.getRegistrationNumber()); //get law trancript by id MONGO
            log.info("inside loop parse json {}", new Date());
            getVotes(law.getRegistrationNumber());
        }
    }
    
    
    @Override
    public void downloadLaws() { //start point
        result.setLast_creation_date(daemonDAO.getResult());
        log.info(result.getLast_creation_date().toString());       
        int count = 1; //page number        
        try {            
            result.setResult(1);
            result.setInfo("OK");
            result.setStart_date(new Date());
            result.setLast_creation_date(daemonDAO.getResult());
            searchUrl += formatter.format(result.getLast_creation_date());
            insertClasses(); //mongo
            insertTopics(); //mongo
            do {                
                log.info("Start download page number {}", count);
                do {
                    try {
                        Thread.sleep(300);
                        ListenableFuture<ResponseEntity<String>> listener = rest.exchange(searchUrl + "&sort=date_asc" + "&page=" + count, HttpMethod.GET, null, String.class);
                        responseEntity = listener.get(5, TimeUnit.SECONDS);
                    } catch (NullPointerException | TimeoutException | InterruptedException | RestClientException | ExecutionException e) {
                        log.error(e.getMessage());
                        try {
                            Thread.sleep(700);
                        } catch (InterruptedException ex) {
                            log.error(e.getMessage());
                        }
                    } catch (Exception e){
                        log.error("ALL OF THIS UHAHA");
                        log.error(e.getMessage());
                        try {
                            Thread.sleep(700);
                        } catch (InterruptedException ex) {
                            log.error(e.getMessage());
                        }
                    }
                } while(responseEntity.getStatusCode() != HttpStatus.OK);
                ++respCount;                
                obj = null;
                obj = (JSONObject) parser.parse(responseEntity.getBody());
                jArr = (JSONArray) obj.get("laws");
                if (jArr != null) { // if we not download all laws
                    parseJson();
                }
                log.info("Page number {} was downloaded", count);
                ++count;                    
            } while (jArr != null && count < 150);            
            result.incDate();            
        } catch (NullPointerException | java.text.ParseException | ParseException e) {
            log.error(e.getMessage());
            result.setResult(0);
            result.setInfo(e.getMessage());                
        } finally {
            daemonDAO.insertResult(result);        
        }
        log.info(searchUrl + "&sort=date_asc" + "&page=" + count);
        log.info("Request count: "+respCount);
    }   
        
    
    /*
    For Votes
    */
        
    //download votes 
    public void getVotes(String regName){  
        log.info("inside getVotes {}", new Date());
        try {
            do {
                try {
                    Thread.sleep(300);
                    ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getVotesResultByLaw + regName, HttpMethod.GET, null, String.class);
                    responseEntity = listener.get(5, TimeUnit.SECONDS);
                } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                    log.error(e.getMessage());
                }
            } while (responseEntity.getStatusCode() != HttpStatus.OK);
            obj = null;
            obj = (JSONObject) parser.parse(responseEntity.getBody());
            if (obj == null){
                return;
            }
            tempArr = (JSONArray) obj.get("votes");
            if (tempArr == null || tempArr.isEmpty()){
                return;
            }
            for (Object o : tempArr) {
                do {
                    try {
                        Thread.sleep(300);                                                
                        Long id = Long.parseLong(((JSONObject) o).get("id").toString());
                        ListenableFuture<ResponseEntity<String>> listener = rest.exchange(getVotes + String.valueOf(id) + ".json?" + appToken, HttpMethod.GET, null, String.class);
                        responseEntity = listener.get(5, TimeUnit.SECONDS);
                    } catch (TimeoutException | RestClientException | ExecutionException | InterruptedException e) {
                        log.error(e.getMessage());
                    }
                } while (responseEntity.getStatusCode() != HttpStatus.OK);                
                obj = (JSONObject) parser.parse(responseEntity.getBody());
                if (obj == null){
                    continue;
                }
                mongoDAO.insertVote(obj);
            }
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        log.info("End inside getVotes {}", new Date());
    }            
}
