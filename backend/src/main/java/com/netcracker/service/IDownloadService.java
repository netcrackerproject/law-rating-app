/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.service;

import org.json.simple.parser.ParseException;

/**
 *
 * @author dmitriy
 */
public interface IDownloadService {
    void downloadLaws(); 
    void getVotes(String regName);
    void addVotesByFaction(long faction_id, String fName, long depId) throws ParseException;
    void insertTranscriptDeputy(long id) throws ParseException;
    int getDeputy(long id) throws ParseException;
    void getLawTranscript(String id) throws ParseException;
    void insertTopics() throws ParseException;
    void insertClasses() throws ParseException;
}
