package com.netcracker.service;

import com.netcracker.entity.Law;
import java.util.List;
/**
 *
 * @author dmitriy
 */
public interface ILawService {      
    List<Law> getLawsByDateDesc(int page);
    List<Law> getLawsByDateAsc(int page);
    List<Law> getLawsByNameDesc(int page);        
    List<Law> getLawsByNameAsc(int page);
    List<Law> getLaws(int page, int mask);
    List<Law> getLawsByUser(String email, int page, int mask);
    Law getLaw(String regNum);
    Law getLawByUser(String email, String regNum);
    List<Law> countRating();
    List<Law> getMin();
    List<Law> getMax();
    long likeLaw(long lawId, String email);
    long dislikeLaw(long lawId, String email); 
    void clearCache();
}
