package com.netcracker.service;

import com.netcracker.DAO.MySQL.ILawDAO;
import com.netcracker.entity.Law;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author dmitriy
 */
@Service
public class LawService implements ILawService{        
    
    @Autowired
    private ILawDAO lawDAO;            
    
    @Override
    @Cacheable(value="lawsByDateDESC", key="#page")
    public List<Law> getLawsByDateDesc(int page) {
        return lawDAO.getLawsByDateDesc(page);
    }
    
    @Override
    @Cacheable(value="lawsByDateASC", key="#page")
    public List<Law> getLawsByDateAsc(int page) {
        return lawDAO.getLawsByDateAsc(page);
    }
    
    @Override
    @Cacheable(value="lawsByNameDESC", key="#page")
    public List<Law> getLawsByNameDesc(int page) {
        return lawDAO.getLawsByNameDesc(page);
    }
    
    @Override
    @Cacheable(value="lawsByNameASC", key="#page")
    public List<Law> getLawsByNameAsc(int page) {
        return lawDAO.getLawsByNameAsc(page);
    }
    
    @Override
    public List<Law> getLaws(int page, int mask){
        switch(mask){
            case 0: return getLawsByDateDesc(page);
            case 1: return getLawsByDateAsc(page);                
            case 2: return getLawsByNameAsc(page);
            default: return getLawsByNameDesc(page);
        }        
    }
    
    @Override
    public List<Law> getLawsByUser(String email, int page, int mask){
        List<Law> laws = getLaws(page, mask);
        List<Map<String,Object>> likes = lawDAO.selectLikedLaws(email);
        likes.forEach((_item) -> {
            laws.stream().filter((law) -> (law.getId() == (Integer)_item.get("law_id"))).forEachOrdered((law) -> {
                law.setLike((Integer)_item.get("rate"));
            });
        });
        return laws;
    }
    
    
    @Override
    @Cacheable(value="laws", key="#regNum")
    public Law getLaw(String regNum){
        return lawDAO.getLaw(regNum);
    }
    
    @Override
    public Law getLawByUser(String email, String regNum){
        Law law = getLaw(regNum);
        law.setLike(lawDAO.selectLikedLaw(email, law.getId()));
        return law;
    }

    @Override
    public List<Law> countRating(){
        return lawDAO.countRating();
    }
    
    @Override
    public long likeLaw(long lawId, String email) {
        return lawDAO.likeLaw(lawId, email);    
    }

    @Override
    public long dislikeLaw(long lawId, String email) {
        return lawDAO.dislikeLaw(lawId, email);        
    }    

    @Override
    public List<Law> getMin() {
        return lawDAO.getMin();
    }

    @Override
    public List<Law> getMax() {
        return lawDAO.getMax();
    }
    
    @Override
    @CacheEvict(value = {"lawsByDateDESC","lawsByDateASC","lawsByNameDESC","lawsByNameASC"}, allEntries = true)
    public void clearCache(){
        
    }
}
