package com.netcracker.controllers;

import com.netcracker.entity.Law;
import com.netcracker.entity.Like;
import com.netcracker.service.ILawService;
import com.netcracker.service.IStatService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dmitriy
 */
@CrossOrigin(origins = "*")
@RestController
public class LawController {
    
    private final Logger log = LoggerFactory.getLogger(LawController.class);
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    private IStatService statService;
    
    @Autowired
    private ILawService lawService;
                
    @GetMapping("/laws")
    public List<Law> getLaws(@RequestParam(required = false) String email, @RequestParam(required = true) int page, @RequestParam(required = true) int mask){            
        log.info("Get start. Time is now {}", dateFormat.format(new Date()));
        if (email == null){
            return lawService.getLaws(page, mask);
        }
        return lawService.getLawsByUser(email, page, mask);
    }
    
    @GetMapping("/law")
    public Law getLaw(@RequestParam(required = false) String email, @RequestParam(required = true) String regNum){                    
        if (email == null){
            return lawService.getLaw(regNum);
        }
        return lawService.getLawByUser(email,regNum);
    }
    
    //for rating
    @GetMapping("/rating")
    public List<Law> getRating(@RequestParam(required = true) String params){        
        switch(params){
            case "min":                
                return lawService.getMin();                
            case "max":                
                return lawService.getMax();
            case "rate":                
                return lawService.countRating();
            default:
                //retrun error wrong param
                return null;
        }        
    }
      
    //issue check database
    @PostMapping("/like") 
    public ResponseEntity<Map<String,String>> like(@RequestBody(required = true) Like input){        
        long err;
       /* HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");*/
        Map<String, String> json = new HashMap<>();         
        if (input.isResult() == true){           
            err = lawService.likeLaw(input.getLawId(),input.getEmail());
        } else {            
            err = lawService.dislikeLaw(input.getLawId(),input.getEmail());
        }
        if (err < 0){
            json.put("message","Internal Server error");
            return new ResponseEntity<>(json,HttpStatus.INTERNAL_SERVER_ERROR);            
        }        
        json.put("message","OK");
        return new ResponseEntity<>(json,HttpStatus.OK);
    }       
}
