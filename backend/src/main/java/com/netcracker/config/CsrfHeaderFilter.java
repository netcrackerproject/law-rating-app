/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.config;
import java.io.BufferedReader;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.AccessDeniedException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

@Configurable
public class CsrfHeaderFilter extends OncePerRequestFilter {
    
    private final String X_CSRF_TOKEN = "X-XSRF-TOKEN";            
    private final String token = "884177262754-amrt34h4mmnajfljb0ojlcv7vbqd3tcc.apps.googleusercontent.com";
    
    
    private final Logger log = LoggerFactory.getLogger(CsrfHeaderFilter.class);
    
    //for creation http request       
    private final AsyncRestTemplate rest;
    
    private final HttpHeaders headers;    
    
    //for parsing JSON
    private ResponseEntity<String> responseEntity;
    private final JSONParser parser;    
    private JSONObject obj; //for containing json object
    private final String googleURL = "https://www.googleapis.com/oauth2/v2/tokeninfo?id_token=";        
    
    @Autowired
    private AccessDeniedHandlerImpl accessDeniedHandler;      

    public CsrfHeaderFilter() { 
        this.parser = new JSONParser();
        this.obj = null;
        this.rest = new AsyncRestTemplate();        
        this.headers = new HttpHeaders();
        this.headers.add("Content-Type", "application/json");
        this.rest.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
    }
    
    private boolean checkResponse(String email) throws ParseException{        
        obj = null;
        obj = (JSONObject) parser.parse(responseEntity.getBody());                
        if (obj.containsKey("error_descprition")){            
            return false;
        }        
        if (obj.get("audience").toString().compareTo(token) != 0 || obj.get("issued_to").toString().compareTo(token) != 0){
            return false;
        }                
        if (obj.get("email").toString().compareTo(email) != 0){
            return false;
        }        
        return true;
    }
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, 
		HttpServletResponse response, FilterChain filterChain)
		throws ServletException, IOException {        
        HttpWrapper requestWrapper = new HttpWrapper(request);
        String csrfTokenValue = request.getHeader(X_CSRF_TOKEN);        
        if (csrfTokenValue != null) {
            try {
                obj = (JSONObject) parser.parse(requestWrapper.getBody());
                ListenableFuture<ResponseEntity<String>> listener = rest.exchange(googleURL + csrfTokenValue, HttpMethod.POST, null, String.class);
                responseEntity = listener.get(5, TimeUnit.SECONDS);
                if (!obj.containsKey("email") || !checkResponse((String) obj.get("email"))) {
                    throw new AccessDeniedException("Not valid token!!!");                    
                }                
            } catch (ParseException | InterruptedException | ExecutionException | TimeoutException ex) {                
                log.error(ex.getMessage());
                throw new AccessDeniedException(ex.getMessage());
            }
        } else {
            throw new AccessDeniedException("No token!!!");
        }
        filterChain.doFilter(requestWrapper, response);
    }    
}