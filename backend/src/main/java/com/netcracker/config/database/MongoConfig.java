/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.config.database;

import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.netcracker.service.DownloadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

/**
 *
 * @author dmitriy
 */

@Configuration
public class MongoConfig {
    
    private final Logger log = LoggerFactory.getLogger(DownloadService.class);
    
    @Value("${spring.mongodb.uri}")
    private String databaseURL;   

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {   
       // log.info(databaseURL);
        MongoClientOptions.Builder options = MongoClientOptions.builder();
        options.socketKeepAlive(true);
        return new SimpleMongoDbFactory(new MongoClientURI(databaseURL,options));
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory());        
    }
   
}
