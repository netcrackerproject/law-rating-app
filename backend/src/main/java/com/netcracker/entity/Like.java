/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.entity;

/**
 *
 * @author dmitriy
 */
public class Like {

    private boolean result;
    private String email;
    private long lawId;

    public Like() {
    }

    
    
    public void setResult(boolean result) {
        this.result = result;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLawId(long lawId) {
        this.lawId = lawId;
    }
    
    
    
    public boolean isResult() {
        return result;
    }

    public String getEmail() {
        return email;
    }

    public long getLawId() {
        return lawId;
    }    
}
