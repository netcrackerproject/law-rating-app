/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author dmitriy
 */
public class DaemonResult {
    private Date start_date;
    private int result;
    private String info;
    private Date last_creation_date;
    private final SimpleDateFormat formatter;

    public DaemonResult(){
        formatter = new SimpleDateFormat("yyyy-MM-dd");
    }
    
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setLast_creation_date(Date last_creation_date) {
        this.last_creation_date = last_creation_date;
    }
        
    public Date getStart_date() {
        return start_date;
    }

    public int getResult() {
        return result;
    }

    public String getInfo() {
        return info;
    }

    public Date getLast_creation_date() {
        return last_creation_date;
    }
    
    public void incDate() throws ParseException{
        last_creation_date = formatter.parse(formatter.format(DateUtils.addDays(last_creation_date, 1)));        
    }
    
}
