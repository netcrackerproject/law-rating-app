package com.netcracker.entity;

import java.util.Date;
import java.util.Objects;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author dmitriy
 */
@Document
public class Law {
    private long Id;
    private String registrationNumber; //may contain not only numbers
    private String name;    
    private Date creationDate;
    private String url; 
    private long rate;
    private long like = 0;    
        
    public Law(){
        
    }        

    public Law(long Id, String registrationNumber, String name, Date creationDate, String url, long rate) {
        this.Id = Id;
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.creationDate = creationDate;
        this.url = url;
        this.rate = rate;        
    }
            
    public Law(long Id,String registrationNumber, String name, Date creationDate, String url) {
        this.Id = Id;
        this.registrationNumber = registrationNumber;
        this.name = name;        
        this.creationDate = creationDate;
        this.url = url;        
    }

    public long getLike(){
        return like;
    }
    
    public void setLike(long like){
        this.like = like;
    }
    
    public long getRate() {
        return rate;
    }

    public void setRate(long rate) {
        this.rate = rate;
    }                
    
    public long getId(){
        return Id;
    }
    
    public void setId(long Id){
        this.Id = Id;
    }
    
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getName() {
        return name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getUrl() {
        return url;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setName(String name) {
        this.name = name;
    }    

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int hashCode() {
        long hash = 7;
        hash = 79 * hash + Objects.hashCode(this.registrationNumber);
        hash = 79 * hash + Objects.hashCode(this.name);        
        hash = 79 * hash + Objects.hashCode(this.creationDate);
        hash = 79 * hash + Objects.hashCode(this.url);
        return (int)hash;
    }    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Law other = (Law) obj;
        if (!other.registrationNumber.equals(this.registrationNumber)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }        
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        return Objects.equals(this.creationDate, other.creationDate);
    }        

    @Override
    public String toString() {
        return "Law{" + "registrationNumber=" + registrationNumber + ", name=" + name + ", creationDate=" + creationDate + ", url=" + url + '}';
    }
}